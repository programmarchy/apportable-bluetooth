//
//  CharacteristicListItem.m
//  Bluetooth
//
//  Created by Donald Ness on 1/5/15.
//  Copyright (c) 2015 Modular Robotics. All rights reserved.
//

#import "CharacteristicListItem.h"
#import "CharacteristicList.h"

@interface CharacteristicListItem ()

@property (strong, nonatomic) CCNodeColor *background;

@end

@implementation CharacteristicListItem

- (id)init
{
    self = [super init];
    if (self) {
        self.userInteractionEnabled = YES;
        self.index = 0;
    }
    return self;
}

- (CharacteristicList *)characteristicList
{
    return (CharacteristicList *)self.parent.parent;
}

#pragma mark - Touch

- (void)touchReadButton
{
    [self.characteristicList.delegate characteristicList:self.characteristicList didReadItemAtIndex:self.index];
}

- (void)touchWriteButton
{
    [self.characteristicList.delegate characteristicList:self.characteristicList didWriteItemAtIndex:self.index];
}

- (void)touchNotifyButton
{
    [self.characteristicList.delegate characteristicList:self.characteristicList didNotifyItemAtIndex:self.index];
}

@end
