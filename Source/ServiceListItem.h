//
//  ServiceListItem.h
//  Bluetooth
//
//  Created by Donald Ness on 1/2/15.
//  Copyright (c) 2015 Modular Robotics. All rights reserved.
//

#import "CCNode.h"

@interface ServiceListItem : CCNode

@property (assign, nonatomic) NSUInteger index;
@property (strong, nonatomic) CCLabelTTF *nameLabel;

@end