//
//  MainScene.m
//  Bluetooth
//
//  Created by Donald Ness on 12/24/14.
//  Copyright (c) 2014 Modular Robotics. All rights reserved.
//

#import "MainScene.h"
#import "DeviceList.h"
#import "DeviceListItem.h"
#import "ServiceList.h"
#import "ServiceListItem.h"
#import "CharacteristicList.h"
#import "CharacteristicListItem.h"
#import <CoreBluetooth/CoreBluetooth.h>

@interface MainScene () <
    CBCentralManagerDelegate, CBPeripheralDelegate,
    DeviceListDataSource, DeviceListDelegate,
    ServiceListDataSource, ServiceListDelegate,
    CharacteristicListDataSource, CharacteristicListDelegate
>

@property (strong, nonatomic) CCNode *scanScreen;
@property (strong, nonatomic) CCButton *scanButton;
@property (strong, nonatomic) CCButton *stopScanButton;
@property (strong, nonatomic) DeviceList *deviceList;

@property (strong, nonatomic) CCNode *deviceScreen;
@property (strong, nonatomic) CCButton *disconnectButton;
@property (strong, nonatomic) ServiceList *serviceList;

@property (strong, nonatomic) CCNode *serviceScreen;
@property (strong, nonatomic) CCButton *backToDeviceScreenButton;
@property (strong, nonatomic) CharacteristicList *characteristicList;

@property (strong, nonatomic) CCNode *readScreen;
@property (strong, nonatomic) CCLabelTTF *readLabel;
@property (strong, nonatomic) CCNode *writeScreen;
@property (strong, nonatomic) CCLabelTTF *writeLabel;
@property (strong, nonatomic) CCNode *notifyScreen;
@property (strong, nonatomic) CCLabelTTF *notifyLabel;
@property (strong, nonatomic) CCButton *notifyButton;

@property (strong, nonatomic) CBCentralManager *centralManager;
@property (strong, nonatomic) dispatch_queue_t eventQueue;
@property (strong, nonatomic) NSMutableArray *peripherals;
@property (strong, nonatomic) CBPeripheral *selectedPeripheral;
@property (strong, nonatomic) CBService *selectedService;
@property (strong, nonatomic) CBCharacteristic *selectedCharacteristic;
@property (strong, nonatomic) NSMutableString *writeString;

@end

@implementation MainScene

- (id)init
{
    self = [super init];
    if (self) {
        self.eventQueue = dispatch_get_main_queue();
        self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:self.eventQueue];
        self.peripherals = [[NSMutableArray alloc] init];
        self.writeString = [NSMutableString stringWithString:@""];
    }
    return self;
}

- (void)didLoadFromCCB
{
    self.scanScreen.visible = YES;
    self.scanButton.enabled = NO;
    self.scanButton.visible = YES;
    self.stopScanButton.visible = NO;
    self.deviceList.dataSource = self;
    self.deviceList.delegate = self;
    
    self.deviceScreen.visible = NO;
    self.serviceList.dataSource = self;
    self.serviceList.delegate = self;
    
    self.serviceScreen.visible = NO;
    self.characteristicList.dataSource = self;
    self.characteristicList.delegate = self;
    
    self.readScreen.visible = NO;
    self.writeScreen.visible = NO;
    self.notifyScreen.visible = NO;
}

- (void)onEnter
{
    [super onEnter];
    [self.deviceList reloadData];
}

#pragma mark - Scan Screen

- (void)touchScanButton
{
    self.scanButton.visible = NO;
    self.stopScanButton.visible = YES;
    [self startScan];
}

- (void)touchStopScanButton
{
    self.scanButton.visible = YES;
    self.stopScanButton.visible = NO;
    [self stopScan];
}

- (void)startScan
{
    [self.centralManager scanForPeripheralsWithServices:nil options:@{
        CBCentralManagerScanOptionAllowDuplicatesKey: @NO
    }];
}

- (void)stopScan
{
    [self.centralManager stopScan];
}

#pragma mark - Device Screen

- (void)showDeviceScreen
{
    self.deviceScreen.visible = YES;
}

- (void)hideDeviceScreen
{
    self.deviceScreen.visible = NO;
}

- (void)touchDisconnectButton
{
    [self.centralManager cancelPeripheralConnection:self.selectedPeripheral];
    [self hideDeviceScreen];
    self.selectedPeripheral = nil;
    [self.serviceList reloadData];
}

#pragma mark - Service Screen

- (void)showServiceScreen
{
    self.serviceScreen.visible = YES;
}

- (void)hideServiceScreen
{
    self.serviceScreen.visible = NO;
}

- (void)touchBackToDeviceScreenButton
{
    [self hideServiceScreen];
    self.selectedService = nil;
    [self.characteristicList reloadData];
}

#pragma mark - Read Screen

- (void)showReadScreen
{
    self.readScreen.visible = YES;
}

- (void)hideReadScreen
{
    self.readScreen.visible = NO;
}

- (void)touchReadButton
{
    [self.selectedPeripheral readValueForCharacteristic:self.selectedCharacteristic];
}

- (void)reloadReadData
{
    self.readLabel.string = self.selectedCharacteristic.value == nil ? @"?" :
        [self hexStringFromData:self.selectedCharacteristic.value];
}

#pragma mark - Write Screen

- (void)showWriteScreen
{
    self.writeScreen.visible = YES;
}

- (void)hideWriteScreen
{
    self.writeScreen.visible = NO;
}

- (void)clearWriteData
{
    self.writeString = [NSMutableString stringWithString:(self.selectedCharacteristic.value == nil ? @"?" :
        [self hexStringFromData:self.selectedCharacteristic.value])];
}

- (void)reloadWriteData
{
    self.writeLabel.string = self.writeString;
}

- (void)appendToWriteString:(NSString *)aString
{
    if ([self.writeString length] < 40) {
        [self.writeString appendString:aString];
        [self reloadWriteData];
    }
}

- (void)touch0Button { [self appendToWriteString:@"0"]; }
- (void)touch1Button { [self appendToWriteString:@"1"]; }
- (void)touch2Button { [self appendToWriteString:@"2"]; }
- (void)touch3Button { [self appendToWriteString:@"3"]; }
- (void)touch4Button { [self appendToWriteString:@"4"]; }
- (void)touch5Button { [self appendToWriteString:@"5"]; }
- (void)touch6Button { [self appendToWriteString:@"6"]; }
- (void)touch7Button { [self appendToWriteString:@"7"]; }
- (void)touch8Button { [self appendToWriteString:@"8"]; }
- (void)touch9Button { [self appendToWriteString:@"9"]; }
- (void)touchAButton { [self appendToWriteString:@"A"]; }
- (void)touchBButton { [self appendToWriteString:@"B"]; }
- (void)touchCButton { [self appendToWriteString:@"C"]; }
- (void)touchDButton { [self appendToWriteString:@"D"]; }
- (void)touchEButton { [self appendToWriteString:@"E"]; }
- (void)touchFButton { [self appendToWriteString:@"F"]; }

- (void)touchBackspaceButton
{
    if ([self.writeString length] > 0) {
        [self.writeString deleteCharactersInRange:NSMakeRange(self.writeString.length - 1, 1)];
        [self reloadWriteData];
    }
}

- (void)touchWriteButton
{
    if ([self.writeString length] % 2 == 0 && [self.writeString length] > 0) {
        NSData *data = [self dataFromHexString:self.writeString];
        CBCharacteristic *characteristic = self.selectedCharacteristic;
        CBCharacteristicProperties properties = characteristic.properties;
        if (properties & CBCharacteristicPropertyWrite) {
            [self.selectedPeripheral writeValue:data forCharacteristic:characteristic
                type:CBCharacteristicWriteWithResponse];
        }
        else if (properties & CBCharacteristicPropertyWriteWithoutResponse) {
            [self.selectedPeripheral writeValue:data forCharacteristic:characteristic
                type:CBCharacteristicWriteWithoutResponse];
        }
    }
    else {
        NSLog(@"Not a valid hex string!");
    }
}

#pragma mark - Notify Screen

- (void)showNotifyScreen
{
    self.notifyScreen.visible = YES;
}

- (void)hideNotifyScreen
{
    self.notifyScreen.visible = NO;
}

- (void)reloadNotifyData
{
    self.notifyLabel.string = self.selectedCharacteristic.value == nil ? @"?" :
        [self hexStringFromData:self.selectedCharacteristic.value];
}

- (void)reloadNotifyButton
{
    BOOL isNotifying = self.selectedCharacteristic.isNotifying;
    self.notifyButton.selected = isNotifying;
    self.notifyButton.label.string = isNotifying ? @"Disable" : @"Enable";
    self.notifyButton.userInteractionEnabled = YES;
}

- (void)touchNotifyButton
{
    self.notifyButton.userInteractionEnabled = NO;
    CBCharacteristic *characteristic = self.selectedCharacteristic;
    BOOL isNotifying = characteristic.isNotifying;
    [self.selectedPeripheral setNotifyValue:!isNotifying forCharacteristic:characteristic];
}

#pragma mark - Back Button

- (void)touchBackToServiceScreenButton
{
    [self hideReadScreen];
    [self hideWriteScreen];
    [self hideNotifyScreen];
}

#pragma mark - Helpers

- (void)addPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"begin add peripheral");
    peripheral.delegate = self;
    BOOL objectExists = NO;
    for (CBPeripheral *p in self.peripherals) {
        if ([p.identifier isEqual:peripheral.identifier]) {
            objectExists = YES;
            break;
        }
    }
    if (!objectExists) {
        [self.peripherals addObject:peripheral];
    }
    NSLog(@"end add peripheral");
}

- (CBPeripheral *)peripheralAtIndex:(NSUInteger)index
{
    return [self.peripherals objectAtIndex:index];
}

- (NSString *)hexStringFromData:(NSData *)theData
{
    NSUInteger length = [theData length];
    NSMutableString *string = [NSMutableString stringWithCapacity:length * 2];
    const unsigned char *bytes = [theData bytes];
    for (NSUInteger i = 0; i < length; ++i) {
        [string appendFormat:@"%02x", bytes[i]];
    }
    return string;
}

- (NSData *)dataFromHexString:(NSString *)hexString
{
    NSMutableData *mutableData = [[NSMutableData alloc] init];
    unsigned char hexByte;
    char hexByteChars[3] = {0x00, 0x00, 0x00};
    for (NSUInteger i = 0; i < ([hexString length] / 2); i++) {
        hexByteChars[0] = [hexString characterAtIndex:i * 2];
        hexByteChars[1] = [hexString characterAtIndex:i * 2 + 1];
        hexByte = strtol(hexByteChars, NULL, 16);
        [mutableData appendBytes:&hexByte length:1]; 
    }
    return [NSData dataWithData:mutableData];
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    switch (central.state) {
        case CBCentralManagerStatePoweredOn:
            self.scanButton.enabled = YES;
            break;
        default:
            self.scanButton.enabled = NO;
            break;
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    NSLog(@"did discover peripheral");
    [self addPeripheral:peripheral];
    [self.deviceList reloadData];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"did connect");
    [self showDeviceScreen];
    [peripheral discoverServices:nil];
    NSLog(@"discover services");
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"did fail to connect: %@", error);
    [self hideDeviceScreen];
    self.selectedPeripheral = nil;
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"disconnected peripheral: %@ error: %@", peripheral, error);
    [self hideReadScreen];
    [self hideWriteScreen];
    [self hideNotifyScreen];
    [self hideServiceScreen];
    [self hideDeviceScreen];
    self.selectedCharacteristic = nil;
    self.selectedService = nil;
    self.selectedPeripheral = nil;
}

#pragma mark - CBPeripheral Delegate

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if (error) {
        NSLog(@"error discovering services: %@", error);
    }
    else {
        NSLog(@"did discover services");
        [self.serviceList reloadData];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if (error) {
        NSLog(@"error discovering characteristics: %@", error);
    }
    else {
        NSLog(@"did discover characteristics");
        [self.characteristicList reloadData];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"error reading characteristic: %@", error);
    }
    else if (characteristic == self.selectedCharacteristic) {
        NSLog(@"did read selected characteristic");
        [self reloadReadData];
        [self reloadNotifyData];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"error writing characteristic: %@", error);
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"error updating notification state: %@", error);
    }
    else if (characteristic == self.selectedCharacteristic) {
        NSLog(@"updated notification state");
        [self reloadNotifyButton];
    }
}

#pragma mark - Device List Data Source

- (NSUInteger)numberOfItemsForDeviceList:(DeviceList *)deviceList
{
    return [self.peripherals count];
}

- (DeviceListItem *)itemForDeviceList:(DeviceList *)deviceList atIndex:(NSUInteger)index
{
    DeviceListItem *item = (DeviceListItem *)[CCBReader load:@"DeviceListItem"];
    item.index = index;
    CBPeripheral *peripheral = [self peripheralAtIndex:index];
    item.nameLabel.string = peripheral.name ? peripheral.name : @"(null)";
    return item;
}

#pragma mark - Device List Delegate

- (void)deviceList:(DeviceList *)deviceList didConnectItemAtIndex:(NSUInteger)index
{
    self.selectedPeripheral = [self peripheralAtIndex:index];
    NSLog(@"connecting to: %@", self.selectedPeripheral.name);
    [self.centralManager connectPeripheral:self.selectedPeripheral options:nil];
    // Show device screen upon connecting
    // @see centralManager:didConnectPeripheral:
}

#pragma mark - Service List Data Source

- (NSUInteger)numberOfItemsForServiceList:(ServiceList *)serviceList
{
    return [self.selectedPeripheral.services count];
}

- (ServiceListItem *)itemForServiceList:(ServiceList *)serviceList atIndex:(NSUInteger)index
{
    ServiceListItem *item = (ServiceListItem *)[CCBReader load:@"ServiceListItem"];
    item.index = index;
    CBService *service = [[self.selectedPeripheral services] objectAtIndex:index];
    item.nameLabel.string = [service.UUID UUIDString];
    return item;
}

#pragma mark - Service List Delegate

- (void)serviceList:(ServiceList *)serviceList didDiscoverItemAtIndex:(NSUInteger)index
{
    self.selectedService = [self.selectedPeripheral.services objectAtIndex:index];
    [self.selectedPeripheral discoverCharacteristics:nil forService:self.selectedService];
    [self showServiceScreen];
}

#pragma mark - Characteristic List Data Source

- (NSUInteger)numberOfItemsForCharacteristicList:(CharacteristicList *)characteristicList
{
    return [self.selectedService.characteristics count];
}

- (CharacteristicListItem *)itemForCharacteristicList:(CharacteristicList *)characteristicList atIndex:(NSUInteger)index
{
    CharacteristicListItem *item = (CharacteristicListItem *)[CCBReader load:@"CharacteristicListItem"];
    item.index = index;
    CBCharacteristic *characteristic = [[self.selectedService characteristics] objectAtIndex:index];
    item.nameLabel.string = [characteristic.UUID UUIDString];
    CBCharacteristicProperties properties = characteristic.properties;
    item.readButton.visible = (BOOL)(properties & CBCharacteristicPropertyRead);
    item.writeButton.visible = (BOOL)((properties & CBCharacteristicPropertyWrite) | (properties & CBCharacteristicPropertyWriteWithoutResponse));
    item.notifyButton.visible = (BOOL)(properties & CBCharacteristicPropertyNotify);
    return item;
}

#pragma mark - Characteristic List Delegate

- (void)characteristicList:(CharacteristicList *)characteristicList didReadItemAtIndex:(NSUInteger)index
{
    self.selectedCharacteristic = [self.selectedService.characteristics objectAtIndex:index];
    [self reloadReadData];
    [self showReadScreen];
}

- (void)characteristicList:(CharacteristicList *)characteristicList didWriteItemAtIndex:(NSUInteger)index
{
    self.selectedCharacteristic = [self.selectedService.characteristics objectAtIndex:index];
    [self clearWriteData];
    [self reloadWriteData];
    [self showWriteScreen];
}

- (void)characteristicList:(CharacteristicList *)characteristicList didNotifyItemAtIndex:(NSUInteger)index
{
    self.selectedCharacteristic = [self.selectedService.characteristics objectAtIndex:index];
    [self reloadNotifyButton];
    [self reloadNotifyData];
    [self showNotifyScreen];
}

@end
