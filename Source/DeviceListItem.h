//
//  DeviceListItem.h
//  Bluetooth
//
//  Created by Donald Ness on 7/24/14.
//  Copyright (c) 2014 Modular Robotics. All rights reserved.
//

#import "CCNode.h"

@interface DeviceListItem : CCNode

@property (assign, nonatomic) NSUInteger index;
@property (strong, nonatomic) CCLabelTTF *nameLabel;

@end
