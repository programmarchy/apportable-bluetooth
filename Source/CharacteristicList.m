//
//  CharacteristicList.m
//  Bluetooth
//
//  Created by Donald Ness on 1/5/15.
//  Copyright (c) 2015 Modular Robotics. All rights reserved.
//

#import "CharacteristicList.h"
#import "CharacteristicListItem.h"

@interface CharacteristicList ()

@property (strong, nonatomic) CCLayoutBox *layoutBox;

@end

@implementation CharacteristicList

- (id)init
{
    self = [super init];
    if (self) {
        self.horizontalScrollEnabled = NO;
        self.verticalScrollEnabled = YES;
        
        self.layoutBox = [[CCLayoutBox alloc] init];
        self.layoutBox.positionType = CCPositionTypeNormalized;
        self.layoutBox.position = ccp(0, 0);
        self.layoutBox.direction = CCLayoutBoxDirectionVertical;
        self.layoutBox.spacing = 1;
        self.layoutBox.anchorPoint = ccp(0.5, 0.5);
        
        self.contentNode = self.layoutBox;
    }
    return self;
}

- (void)onEnter
{
    [super onEnter];
    [self reloadData];
}

- (void)setVisible:(BOOL)visible
{
    [super setVisible:visible];
    [super setUserInteractionEnabled:visible];
    [self.layoutBox setVisible:visible];
    [self.layoutBox setUserInteractionEnabled:visible];
}

- (void)reloadData
{
    // Clear any existing children
    [self.layoutBox removeAllChildrenWithCleanup:YES];
    
    // Add children from data source
    NSUInteger numberOfItems = [self.dataSource numberOfItemsForCharacteristicList:self];
    for (NSUInteger i = 0; i < numberOfItems; ++i) {
        CharacteristicListItem *item = [self.dataSource itemForCharacteristicList:self atIndex:i];
        if (item) {
            [self.layoutBox addChild:item];
        }
    }
}

@end
