//
//  DeviceListItem.m
//  Bluetooth
//
//  Created by Donald Ness on 7/24/14.
//  Copyright (c) 2014 Modular Robotics. All rights reserved.
//

#import "DeviceListItem.h"
#import "DeviceList.h"

@interface DeviceListItem ()

@property (strong, nonatomic) CCNodeColor *background;

@end

@implementation DeviceListItem

- (id)init
{
    self = [super init];
    if (self) {
        self.userInteractionEnabled = YES;
        self.index = 0;
    }
    return self;
}

- (DeviceList *)deviceList
{
    return (DeviceList *)self.parent.parent;
}

#pragma mark - Touch

- (void)touchConnectButton
{
    [self.deviceList.delegate deviceList:self.deviceList didConnectItemAtIndex:self.index];
}

@end
