//
//  ServiceListItem.m
//  Bluetooth
//
//  Created by Donald Ness on 1/2/15.
//  Copyright (c) 2015 Modular Robotics. All rights reserved.
//

#import "ServiceListItem.h"
#import "ServiceList.h"

@interface ServiceListItem ()

@property (strong, nonatomic) CCNodeColor *background;

@end

@implementation ServiceListItem

- (id)init
{
    self = [super init];
    if (self) {
        self.userInteractionEnabled = YES;
        self.index = 0;
    }
    return self;
}

- (ServiceList *)serviceList
{
    return (ServiceList *)self.parent.parent;
}

#pragma mark - Touch

- (void)touchDiscoverButton
{
    [self.serviceList.delegate serviceList:self.serviceList didDiscoverItemAtIndex:self.index];
}

@end
