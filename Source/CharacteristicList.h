//
//  CharacteristicList.h
//  Bluetooth
//
//  Created by Donald Ness on 1/5/15.
//  Copyright (c) 2015 Modular Robotics. All rights reserved.
//

#import "CCScrollView.h"

@class CharacteristicList;
@class CharacteristicListItem;

@protocol CharacteristicListDataSource <NSObject>

- (NSUInteger)numberOfItemsForCharacteristicList:(CharacteristicList *)characteristicList;
- (CharacteristicListItem *)itemForCharacteristicList:(CharacteristicList *)characteristicList atIndex:(NSUInteger)index;

@end

@protocol CharacteristicListDelegate <CCScrollViewDelegate>

- (void)characteristicList:(CharacteristicList *)characteristicList didReadItemAtIndex:(NSUInteger)index;
- (void)characteristicList:(CharacteristicList *)characteristicList didWriteItemAtIndex:(NSUInteger)index;
- (void)characteristicList:(CharacteristicList *)characteristicList didNotifyItemAtIndex:(NSUInteger)index;

@end

@interface CharacteristicList : CCScrollView

@property (weak, nonatomic) id <CharacteristicListDelegate> delegate;
@property (weak, nonatomic) id <CharacteristicListDataSource> dataSource;

- (void)reloadData;

@end
