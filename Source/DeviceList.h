//
//  DeviceList.h
//  Bluetooth
//
//  Created by Donald Ness on 7/23/14.
//  Copyright (c) 2014 Modular Robotics. All rights reserved.
//

#import "CCScrollView.h"

@class DeviceList;
@class DeviceListItem;

@protocol DeviceListDataSource <NSObject>

- (NSUInteger)numberOfItemsForDeviceList:(DeviceList *)deviceList;
- (DeviceListItem *)itemForDeviceList:(DeviceList *)deviceList atIndex:(NSUInteger)index;

@end

@protocol DeviceListDelegate <CCScrollViewDelegate>

- (void)deviceList:(DeviceList *)deviceList didConnectItemAtIndex:(NSUInteger)index;

@end

@interface DeviceList : CCScrollView

@property (weak, nonatomic) id <DeviceListDelegate> delegate;
@property (weak, nonatomic) id <DeviceListDataSource> dataSource;

- (void)reloadData;

@end
