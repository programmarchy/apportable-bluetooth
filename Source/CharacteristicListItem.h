//
//  CharacteristicListItem.h
//  Bluetooth
//
//  Created by Donald Ness on 1/5/15.
//  Copyright (c) 2015 Modular Robotics. All rights reserved.
//

#import "CCNode.h"

@interface CharacteristicListItem : CCNode

@property (assign, nonatomic) NSUInteger index;
@property (strong, nonatomic) CCLabelTTF *nameLabel;
@property (strong, nonatomic) CCButton *readButton;
@property (strong, nonatomic) CCButton *writeButton;
@property (strong, nonatomic) CCButton *notifyButton;

@end