//
//  DeviceList.m
//  Bluetooth
//
//  Created by Donald Ness on 7/23/14.
//  Copyright (c) 2014 Modular Robotics. All rights reserved.
//

#import "DeviceList.h"
#import "DeviceListItem.h"

@interface DeviceList ()

@property (strong, nonatomic) CCLayoutBox *layoutBox;

@end

@implementation DeviceList

- (id)init
{
    self = [super init];
    if (self) {
        self.horizontalScrollEnabled = NO;
        self.verticalScrollEnabled = YES;
        
        self.layoutBox = [[CCLayoutBox alloc] init];
        self.layoutBox.positionType = CCPositionTypeNormalized;
        self.layoutBox.position = ccp(0, 0);
        self.layoutBox.direction = CCLayoutBoxDirectionVertical;
        self.layoutBox.spacing = 1;
        self.layoutBox.anchorPoint = ccp(0.5, 0.5);
        
        self.contentNode = self.layoutBox;
    }
    return self;
}

- (void)onEnter
{
    [super onEnter];
    [self reloadData];
}

- (void)setVisible:(BOOL)visible
{
    [super setVisible:visible];
    [super setUserInteractionEnabled:visible];
    [self.layoutBox setVisible:visible];
    [self.layoutBox setUserInteractionEnabled:visible];
}

- (void)reloadData
{
    // Clear any existing children
    [self.layoutBox removeAllChildrenWithCleanup:YES];
    
    // Add children from data source
    NSUInteger numberOfItems = [self.dataSource numberOfItemsForDeviceList:self];
    for (NSUInteger i = 0; i < numberOfItems; ++i) {
        DeviceListItem *item = [self.dataSource itemForDeviceList:self atIndex:i];
        if (item) {
            [self.layoutBox addChild:item];
        }
    }
}

@end
