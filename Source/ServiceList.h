//
//  ServiceList.h
//  Bluetooth
//
//  Created by Donald Ness on 1/2/15.
//  Copyright (c) 2015 Modular Robotics. All rights reserved.
//

#import "CCScrollView.h"

@class ServiceList;
@class ServiceListItem;

@protocol ServiceListDataSource <NSObject>

- (NSUInteger)numberOfItemsForServiceList:(ServiceList *)serviceList;
- (ServiceListItem *)itemForServiceList:(ServiceList *)serviceList atIndex:(NSUInteger)index;

@end

@protocol ServiceListDelegate <CCScrollViewDelegate>

- (void)serviceList:(ServiceList *)serviceList didDiscoverItemAtIndex:(NSUInteger)index;

@end

@interface ServiceList : CCScrollView

@property (weak, nonatomic) id <ServiceListDelegate> delegate;
@property (weak, nonatomic) id <ServiceListDataSource> dataSource;

- (void)reloadData;

@end
