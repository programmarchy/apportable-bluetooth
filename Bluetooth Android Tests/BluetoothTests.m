//
//  BluetoothTests
//  Bluetooth Tests
//
//  Created by Donald Ness on 1/2/15.
//  Copyright (c) 2015 Modular Robotics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <CoreBluetooth/CBUUID.h>

@interface BluetoothTests : XCTestCase

@end

@implementation BluetoothTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testLittleEndian
{
    int n = 1;
    XCTAssertTrue((*(char *)&n == 1), @"System should be little endian.");
}

- (void)testCBUUIDToAndFromData
{
    const uint8_t bytes[] = { 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x10, 0x00, 0x80, 0x00, 0x00, 0x80, 0x5f, 0x9b, 0x34, 0xfb };
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    NSString *s = @"00001800-0000-1000-8000-00805f9b34fb";
    CBUUID *UUID0 = [CBUUID UUIDWithData:data];
    XCTAssertEqualObjects(s, [UUID0 UUIDString]);
    CBUUID *UUID1 = [CBUUID UUIDWithString:s];
    XCTAssertEqualObjects(s, [UUID1 UUIDString]);
    XCTAssertTrue([UUID0 isEqual:UUID1]);
}

- (void)testCBUUIDFromBits
{
    const uint64_t bytes0[] = { CFSwapInt64(26388279070720), CFSwapInt64(-9223371485494954757) };
    NSData *data0 = [NSData dataWithBytes:bytes0 length:sizeof(bytes0)];
    CBUUID *UUID0 = [CBUUID UUIDWithData:data0];
    NSString *s0 = @"00001800-0000-1000-8000-00805f9b34fb";
    XCTAssertEqualObjects(s0, [UUID0 UUIDString]);
    
    const uint64_t bytes1[] = { CFSwapInt64(26392574038016), CFSwapInt64(-9223371485494954757) };
    NSData *data1 = [NSData dataWithBytes:bytes1 length:sizeof(bytes1)];
    CBUUID *UUID1 = [CBUUID UUIDWithData:data1];
    NSString *s1 = @"00001801-0000-1000-8000-00805f9b34fb";
    XCTAssertEqualObjects(s1, [UUID1 UUIDString]);
    
    const uint64_t bytes2[] = { CFSwapInt64(3482689892435059794), CFSwapInt64(8026100031702659873) };
    NSData *data2 = [NSData dataWithBytes:bytes2 length:sizeof(bytes2)];
    CBUUID *UUID2 = [CBUUID UUIDWithData:data2];
    NSString *s2 = @"30550001-4d6f-6452-6f62-6f7469637321";
    XCTAssertEqualObjects(s2, [UUID2 UUIDString]);
    
    const uint64_t bytes3[] = { CFSwapInt64(23296205844446), CFSwapInt64(1523193452336828707) };
    NSData *data3 = [NSData dataWithBytes:bytes3 length:sizeof(bytes3)];
    CBUUID *UUID3 = [CBUUID UUIDWithData:data3];
    NSString *s3 = @"00001530-1212-efde-1523-785feabcd123";
    XCTAssertEqualObjects(s3, [UUID3 UUIDString]);
}

@end
