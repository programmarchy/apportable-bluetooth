#import <BridgeKit/JavaObject.h>

#define AndroidBluetoothGattDescriptorPermissionRead 1
#define AndroidBluetoothGattDescriptorPermissionReadEncrypted 2
#define AndroidBluetoothGattDescriptorPermissionReadEncryptedMITM 4
#define AndroidBluetoothGattDescriptorPermissionWrite 16
#define AndroidBluetoothGattDescriptorPermissionWriteEncrypted 32
#define AndroidBluetoothGattDescriptorPermissionWriteEncryptedMITM 64
#define AndroidBluetoothGattDescriptorPermissionWriteSigned 128
#define AndroidBluetoothGattDescriptorPermissionWriteSignedMITM 256

@class AndroidUUID, AndroidBluetoothGattCharacteristic;

@interface AndroidBluetoothGattDescriptor : JavaObject

+ (NSData *)disableNotificationValue;
+ (NSData *)enableIndicationValue;
+ (NSData *)enableNotificationValue;

- (AndroidUUID *)UUID;
- (AndroidBluetoothGattCharacteristic *)characteristic;
- (int)permissions;
- (NSData *)value;
- (BOOL)setValue:(NSData *)value;

@end
