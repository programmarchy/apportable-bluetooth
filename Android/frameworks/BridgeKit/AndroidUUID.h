#import <BridgeKit/JavaObject.h>

@interface AndroidUUID : JavaObject

+ (AndroidUUID *)fromString:(NSString *)UUIDString;

- (id)initWithMostSignificantBits:(long long)mostSigBits leastSignificantBits:(long long)leastSigBits;

- (long long)leastSignificantBits;
- (long long)mostSignificantBits;

@end
