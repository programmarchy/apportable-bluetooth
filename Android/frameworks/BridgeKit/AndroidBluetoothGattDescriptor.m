#import "AndroidBluetoothGattDescriptor.h"
#import "AndroidBluetoothGattCharacteristic.h"
#import "AndroidUUID.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation AndroidBluetoothGattDescriptor

+ (void)initializeJava
{
    [super initializeJava];

    [AndroidUUID initializeJava];

    [AndroidBluetoothGattDescriptor
        registerStaticField:@"DISABLE_NOTIFICATION_VALUE"
        selector:@selector(disableNotificationValue)
        type:[NSData className]];

    [AndroidBluetoothGattDescriptor
        registerStaticField:@"ENABLE_INDICATION_VALUE"
        selector:@selector(enableIndicationValue)
        type:[NSData className]];

    [AndroidBluetoothGattDescriptor
        registerStaticField:@"ENABLE_NOTIFICATION_VALUE"
        selector:@selector(enableNotificationValue)
        type:[NSData className]];

    [AndroidBluetoothGattDescriptor
        registerInstanceMethod:@"getUuid"
        selector:@selector(UUID)
        returnValue:[AndroidUUID className]];

    [AndroidBluetoothGattDescriptor
        registerInstanceMethod:@"getCharacteristic"
        selector:@selector(characteristic)
        returnValue:[AndroidBluetoothGattCharacteristic className]];

    [AndroidBluetoothGattDescriptor
        registerInstanceMethod:@"getPermissions"
        selector:@selector(permissions)
        returnValue:[JavaClass intPrimitive]];

    [AndroidBluetoothGattDescriptor
        registerInstanceMethod:@"getValue"
        selector:@selector(value)
        returnValue:[NSData className]];

    [AndroidBluetoothGattDescriptor
        registerInstanceMethod:@"setValue"
        selector:@selector(setValue:)
        returnValue:[JavaClass boolPrimitive]
        arguments:[NSData className], nil];
}

+ (NSString *)className
{
    return @"android.bluetooth.BluetoothGattDescriptor";
}

@end

#pragma clang diagnostic pop