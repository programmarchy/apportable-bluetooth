#import <BridgeKit/JavaObject.h>
#import <BridgeKit/JavaList.h>

#define AndroidBluetoothGattCharacteristicPermissionRead 1
#define AndroidBluetoothGattCharacteristicPermissionReadEncrypted 2
#define AndroidBluetoothGattCharacteristicPermissionReadEncryptedMITM 4
#define AndroidBluetoothGattCharacteristicPermissionWrite 16
#define AndroidBluetoothGattCharacteristicPermissionWriteEncrypted 32
#define AndroidBluetoothGattCharacteristicPermissionWriteEncryptedMITM 64
#define AndroidBluetoothGattCharacteristicPermissionWriteSigned 128
#define AndroidBluetoothGattCharacteristicPermissionWriteSignedMITM 256
#define AndroidBluetoothGattCharacteristicPropertyBroadcast 1
#define AndroidBluetoothGattCharacteristicPropertyExtendedProps 128
#define AndroidBluetoothGattCharacteristicPropertyIndicate 32
#define AndroidBluetoothGattCharacteristicPropertyNotify 16
#define AndroidBluetoothGattCharacteristicPropertyRead 2
#define AndroidBluetoothGattCharacteristicPropertySignedWrite 64
#define AndroidBluetoothGattCharacteristicPropertyWrite 8
#define AndroidBluetoothGattCharacteristicPropertyWriteNoResponse 4
#define AndroidBluetoothGattCharacteristicWriteTypeDefault 2
#define AndroidBluetoothGattCharacteristicWriteTypeNoResponse 1
#define AndroidBluetoothGattCharacteristicWriteTypeSigned 4

@class AndroidUUID, AndroidBluetoothGattService, AndroidBluetoothGattDescriptor;

@interface AndroidBluetoothGattCharacteristic : JavaObject

- (AndroidUUID *)UUID;
- (AndroidBluetoothGattDescriptor *)descriptorForUUID:(AndroidUUID *)UUID;
- (JavaList *)descriptors;
- (int)permissions;
- (int)properties;
- (AndroidBluetoothGattService *)service;
- (NSData *)value;
- (BOOL)setValue:(NSData *)value;
- (int)writeType;
- (void)setWriteType:(int)writeType;

@end
