#import "AndroidBluetoothDevice.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation AndroidBluetoothDevice

+ (void)initializeJava
{
    [super initializeJava];

    [AndroidBluetoothDevice
        registerInstanceMethod:@"getName"
        selector:@selector(name)
        returnValue:[NSString className]];

    [AndroidBluetoothDevice
        registerInstanceMethod:@"getAddress"
        selector:@selector(address)
        returnValue:[NSString className]];

    [AndroidBluetoothDevice
        registerInstanceMethod:@"hashCode"
        selector:@selector(hashCode)
        returnValue:[JavaClass intPrimitive]];
}

+ (NSString *)className
{
    return @"android.bluetooth.BluetoothDevice";
}

@end

#pragma clang diagnostic pop