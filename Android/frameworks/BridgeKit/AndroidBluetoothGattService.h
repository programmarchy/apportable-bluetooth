#import <BridgeKit/JavaObject.h>
#import <BridgeKit/JavaList.h>

#define AndroidBluetoothGattServiceTypePrimary 0
#define AndroidBluetoothGattServiceTypeSecondary 1

@class AndroidUUID, AndroidBluetoothGattCharacteristic;

@interface AndroidBluetoothGattService : JavaObject

- (AndroidUUID *)UUID;
- (AndroidBluetoothGattCharacteristic *)characteristicForUUID:(AndroidUUID *)UUID;
- (JavaList *)characteristics;
- (JavaList *)includedServices;
- (int)type;

@end
