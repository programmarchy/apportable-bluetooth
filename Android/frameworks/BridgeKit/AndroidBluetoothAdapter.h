#import <BridgeKit/JavaObject.h>
#import <BridgeKit/JavaSet.h>

@interface AndroidBluetoothAdapter : JavaObject

+ (AndroidBluetoothAdapter *)defaultAdapter;

- (BOOL)isEnabled;
- (JavaSet *)bondedDevices;

@end
