#import <BridgeKit/JavaObject.h>
#import <BridgeKit/JavaList.h>

#define AndroidBluetoothGattFailure 257
#define AndroidBluetoothGattInsufficientAuthentication 5
#define AndroidBluetoothGattInsufficientEncryption 15
#define AndroidBluetoothGattInvalidAttributeLength 13
#define AndroidBluetoothGattInvalidOffset 7
#define AndroidBluetoothGattReadNotPermitted 2
#define AndroidBluetoothGattRequestNotSupported 6
#define AndroidBluetoothGattSuccess 0
#define AndroidBluetoothGattWriteNotPermitted 3

@class AndroidBluetoothDevice, AndroidBluetoothGattCharacteristic, AndroidBluetoothGattDescriptor;

@interface AndroidBluetoothGatt : JavaObject

- (void)close;
- (BOOL)connect;
- (void)disconnect;
- (BOOL)discoverServices;
- (AndroidBluetoothDevice *)device;
- (JavaList *)services;
- (BOOL)readCharacteristic:(AndroidBluetoothGattCharacteristic *)characteristic;
- (BOOL)writeCharacteristic:(AndroidBluetoothGattCharacteristic *)characteristic;
- (BOOL)setCharacteristicNotification:(AndroidBluetoothGattCharacteristic *)characteristic enabled:(BOOL)enabled;
- (BOOL)readDescriptor:(AndroidBluetoothGattDescriptor *)characteristic;
- (BOOL)writeDescriptor:(AndroidBluetoothGattDescriptor *)characteristic;

@end
