#import "AndroidBluetoothAdapter.h"
#import "AndroidBluetoothDevice.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation AndroidBluetoothAdapter

+ (void)initializeJava
{
    [super initializeJava];

    [AndroidBluetoothDevice initializeJava];

    [AndroidBluetoothAdapter
        registerStaticMethod:@"getDefaultAdapter"
        selector:@selector(defaultAdapter)
        returnValue:[AndroidBluetoothAdapter className]];

    [AndroidBluetoothAdapter
        registerInstanceMethod:@"isEnabled"
        selector:@selector(isEnabled)
        returnValue:[JavaClass boolPrimitive]];

    [AndroidBluetoothAdapter
        registerInstanceMethod:@"getBondedDevices"
        selector:@selector(bondedDevices)
        returnValue:[JavaSet className]];
}

+ (NSString *)className
{
    return @"android.bluetooth.BluetoothAdapter";
}

@end

#pragma clang diagnostic pop