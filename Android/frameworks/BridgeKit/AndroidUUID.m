#import "AndroidUUID.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation AndroidUUID

+ (void)initializeJava
{
    [super initializeJava];

    [AndroidUUID
        registerStaticMethod:@"fromString"
        selector:@selector(fromString:)
        returnValue:[AndroidUUID className]
        arguments:[NSString className], nil];

    [AndroidUUID
        registerConstructorWithSelector:@selector(initWithMostSignificantBits:leastSignificantBits:)
        arguments:[JavaClass longPrimitive], [JavaClass longPrimitive], nil];

    [AndroidUUID
        registerInstanceMethod:@"getLeastSignificantBits"
        selector:@selector(leastSignificantBits)
        returnValue:[JavaClass longPrimitive]];

    [AndroidUUID
        registerInstanceMethod:@"getMostSignificantBits"
        selector:@selector(mostSignificantBits)
        returnValue:[JavaClass longPrimitive]];
}

+ (NSString *)className
{
    return @"java.util.UUID";
}

@end

#pragma clang diagnostic pop