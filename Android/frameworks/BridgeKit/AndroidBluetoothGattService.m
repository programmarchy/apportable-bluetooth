#import "AndroidBluetoothGattService.h"
#import "AndroidBluetoothGattCharacteristic.h"
#import "AndroidUUID.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation AndroidBluetoothGattService

+ (void)initializeJava
{
    [super initializeJava];

    [AndroidUUID initializeJava];
    [AndroidBluetoothGattCharacteristic initializeJava];

    [AndroidBluetoothGattService
        registerInstanceMethod:@"getUuid"
        selector:@selector(UUID)
        returnValue:[AndroidUUID className]];

    [AndroidBluetoothGattService
        registerInstanceMethod:@"getCharacteristic"
        selector:@selector(characteristicForUUID:)
        returnValue:[AndroidBluetoothGattCharacteristic className]
        arguments:[AndroidUUID className], nil];

    [AndroidBluetoothGattService
        registerInstanceMethod:@"getCharacteristics"
        selector:@selector(characteristics)
        returnValue:[JavaList className]];

    [AndroidBluetoothGattService
        registerInstanceMethod:@"getIncludedServices"
        selector:@selector(includedServices)
        returnValue:[JavaList className]];

    [AndroidBluetoothGattService
        registerInstanceMethod:@"getType"
        selector:@selector(type)
        returnValue:[JavaClass intPrimitive]];
}

+ (NSString *)className
{
    return @"android.bluetooth.BluetoothGattService";
}

@end

#pragma clang diagnostic pop