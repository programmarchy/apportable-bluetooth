#import <BridgeKit/JavaObject.h>

@interface AndroidBluetoothDevice : JavaObject

- (NSString *)name;
- (NSString *)address;
- (int)hashCode;

@end
