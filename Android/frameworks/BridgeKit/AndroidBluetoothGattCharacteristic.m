#import "AndroidBluetoothGattCharacteristic.h"
#import "AndroidBluetoothGattService.h"
#import "AndroidBluetoothGattDescriptor.h"
#import "AndroidUUID.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation AndroidBluetoothGattCharacteristic

+ (void)initializeJava
{
    [super initializeJava];

    [AndroidUUID initializeJava];
    [AndroidBluetoothGattDescriptor initializeJava];

    [AndroidBluetoothGattCharacteristic
        registerInstanceMethod:@"getUuid"
        selector:@selector(UUID)
        returnValue:[AndroidUUID className]];

    [AndroidBluetoothGattCharacteristic
        registerInstanceMethod:@"getDescriptor"
        selector:@selector(descriptorForUUID:)
        returnValue:[AndroidBluetoothGattDescriptor className]
        arguments:[AndroidUUID className], nil];

    [AndroidBluetoothGattCharacteristic
        registerInstanceMethod:@"getDescriptors"
        selector:@selector(descriptors)
        returnValue:[JavaList className]];

    [AndroidBluetoothGattCharacteristic
        registerInstanceMethod:@"getPermissions"
        selector:@selector(permissions)
        returnValue:[JavaClass intPrimitive]];

    [AndroidBluetoothGattCharacteristic
        registerInstanceMethod:@"getProperties"
        selector:@selector(properties)
        returnValue:[JavaClass intPrimitive]];

    [AndroidBluetoothGattCharacteristic
        registerInstanceMethod:@"getService"
        selector:@selector(service)
        returnValue:[AndroidBluetoothGattService className]];

    [AndroidBluetoothGattCharacteristic
        registerInstanceMethod:@"getValue"
        selector:@selector(value)
        returnValue:[NSData className]];

    [AndroidBluetoothGattCharacteristic
        registerInstanceMethod:@"setValue"
        selector:@selector(setValue:)
        returnValue:[JavaClass boolPrimitive]
        arguments:[NSData className], nil];

    [AndroidBluetoothGattCharacteristic
        registerInstanceMethod:@"getWriteType"
        selector:@selector(writeType)
        returnValue:[JavaClass intPrimitive]];

    [AndroidBluetoothGattCharacteristic
        registerInstanceMethod:@"setWriteType"
        selector:@selector(setWriteType:)
        arguments:[JavaClass intPrimitive], nil];
}

+ (NSString *)className
{
    return @"android.bluetooth.BluetoothGattCharacteristic";
}

@end

#pragma clang diagnostic pop