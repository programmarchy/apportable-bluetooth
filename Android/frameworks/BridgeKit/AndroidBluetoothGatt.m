#import "AndroidBluetoothGatt.h"
#import "AndroidBluetoothGattService.h"
#import "AndroidBluetoothGattCharacteristic.h"
#import "AndroidBluetoothGattDescriptor.h"
#import "AndroidBluetoothDevice.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation AndroidBluetoothGatt

+ (void)initializeJava
{
    [super initializeJava];

    [AndroidBluetoothGattService initializeJava];

    [AndroidBluetoothGatt
        registerInstanceMethod:@"close"
        selector:@selector(close)];

    [AndroidBluetoothGatt
        registerInstanceMethod:@"connect"
        selector:@selector(connect)
        returnValue:[JavaClass boolPrimitive]];

    [AndroidBluetoothGatt
        registerInstanceMethod:@"disconnect"
        selector:@selector(disconnect)];

    [AndroidBluetoothGatt
        registerInstanceMethod:@"discoverServices"
        selector:@selector(discoverServices)
        returnValue:[JavaClass boolPrimitive]];

    [AndroidBluetoothGatt
        registerInstanceMethod:@"getDevice"
        selector:@selector(device)
        returnValue:[AndroidBluetoothDevice className]];

    [AndroidBluetoothGatt
        registerInstanceMethod:@"getServices"
        selector:@selector(services)
        returnValue:[JavaList className]];

    [AndroidBluetoothGatt
        registerInstanceMethod:@"readCharacteristic"
        selector:@selector(readCharacteristic:)
        returnValue:[JavaClass boolPrimitive]
        arguments:[AndroidBluetoothGattCharacteristic className], nil];

    [AndroidBluetoothGatt
        registerInstanceMethod:@"writeCharacteristic"
        selector:@selector(writeCharacteristic:)
        returnValue:[JavaClass boolPrimitive]
        arguments:[AndroidBluetoothGattCharacteristic className], nil];

    [AndroidBluetoothGatt
        registerInstanceMethod:@"setCharacteristicNotification"
        selector:@selector(setCharacteristicNotification:enabled:)
        returnValue:[JavaClass boolPrimitive]
        arguments:[AndroidBluetoothGattCharacteristic className], [JavaClass boolPrimitive], nil];

    [AndroidBluetoothGatt
        registerInstanceMethod:@"readDescriptor"
        selector:@selector(readDescriptor:)
        returnValue:[JavaClass boolPrimitive]
        arguments:[AndroidBluetoothGattDescriptor className], nil];

    [AndroidBluetoothGatt
        registerInstanceMethod:@"writeDescriptor"
        selector:@selector(writeDescriptor:)
        returnValue:[JavaClass boolPrimitive]
        arguments:[AndroidBluetoothGattDescriptor className], nil];
}

+ (NSString *)className
{
    return @"android.bluetooth.BluetoothGatt";
}

@end

#pragma clang diagnostic pop