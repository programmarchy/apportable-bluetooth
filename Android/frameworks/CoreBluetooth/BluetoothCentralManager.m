#import <CoreBluetooth/BluetoothCentralManager.h>
#import <CoreBluetooth/BluetoothPeripheral.h>
#import <BridgeKit/AndroidBluetoothDevice.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation BluetoothCentralManager

@synthesize delegate;

+ (void)initializeJava
{
    [super initializeJava];

    [BluetoothCentralManager
        registerConstructorWithSelector:@selector(initWithActivity:)
        arguments:[AndroidActivity className], nil];

    [BluetoothCentralManager
        registerInstanceMethod:@"getState"
        selector:@selector(state)
        returnValue:[JavaClass intPrimitive]
        arguments:nil];

    [BluetoothCentralManager
        registerInstanceMethod:@"updateState"
        selector:@selector(updateState)];

    [BluetoothCentralManager
        registerInstanceMethod:@"startScan"
        selector:@selector(startScan)];

    [BluetoothCentralManager
        registerInstanceMethod:@"stopScan"
        selector:@selector(stopScan)];

    [BluetoothCentralManager
        registerInstanceMethod:@"connectPeripheral"
        selector:@selector(connectPeripheral:)
        arguments:[BluetoothPeripheral className], nil];

    [BluetoothCentralManager
        registerInstanceMethod:@"cancelPeripheralConnection"
        selector:@selector(cancelPeripheralConnection:)
        arguments:[BluetoothPeripheral className], nil];

    [BluetoothCentralManager
        registerCallback:@"stateChangedCallback"
        selector:@selector(stateChanged)
        returnValue:nil
        arguments:nil];

    [BluetoothCentralManager
        registerCallback:@"deviceDiscoveredCallback"
        selector:@selector(deviceDiscovered:RSSI:scanRecord:)
        returnValue:nil
        arguments:[AndroidBluetoothDevice className], [JavaClass intPrimitive], [NSData className], nil];

    [BluetoothCentralManager
        registerCallback:@"deviceConnectedCallback"
        selector:@selector(deviceConnected:status:)
        returnValue:nil
        arguments:[AndroidBluetoothDevice className], nil];

    [BluetoothCentralManager
        registerCallback:@"deviceFailedToConnectCallback"
        selector:@selector(deviceFailedToConnect:status:)
        returnValue:nil
        arguments:[AndroidBluetoothDevice className], [JavaClass intPrimitive], nil];

    [BluetoothCentralManager
        registerCallback:@"deviceDisconnectedCallback"
        selector:@selector(deviceDisconnected:status:)
        returnValue:nil
        arguments:[AndroidBluetoothDevice className], [JavaClass intPrimitive], nil];
}

- (void)stateChanged
{
    if ([self.delegate respondsToSelector:@selector(bluetoothCentralManagerStateChanged:)]) {
        [self.delegate bluetoothCentralManagerStateChanged:self];
    }
}

- (void)deviceDiscovered:(AndroidBluetoothDevice *)device RSSI:(int)RSSI scanRecord:(NSData *)scanRecord
{
    if ([self.delegate respondsToSelector:@selector(bluetoothCentralManager:deviceDiscovered:RSSI:scanRecord:)]) {
        [self.delegate bluetoothCentralManager:self deviceDiscovered:device RSSI:RSSI scanRecord:scanRecord];
    }
}

- (void)deviceConnected:(AndroidBluetoothDevice *)device status:(int)status
{
    if ([self.delegate respondsToSelector:@selector(bluetoothCentralManager:deviceConnected:)]) {
        [self.delegate bluetoothCentralManager:self deviceConnected:device];
    }
}

- (void)deviceFailedToConnect:(AndroidBluetoothDevice *)device status:(int)status
{
    if ([self.delegate respondsToSelector:@selector(bluetoothCentralManager:deviceFailedToConnect:status:)]) {
        [self.delegate bluetoothCentralManager:self deviceFailedToConnect:device status:status];
    }
}

- (void)deviceDisconnected:(AndroidBluetoothDevice *)device status:(int)status
{
    if ([self.delegate respondsToSelector:@selector(bluetoothCentralManager:deviceDisconnected:status:)]) {
        [self.delegate bluetoothCentralManager:self deviceDisconnected:device status:status];
    }
}

+ (NSString *)className
{
    return @"com.modrobotics.coreBluetooth.BluetoothCentralManager";
}

@end

#pragma clang diagnostic pop