#import <CoreBluetooth/CBDefines.h>
#import <CoreBluetooth/CBError.h>
#import <CoreBluetooth/CBPeripheralManagerConstants.h>
#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, CBPeripheralManagerAuthorizationStatus) {
    CBPeripheralManagerAuthorizationStatusNotDetermined = 0,
    CBPeripheralManagerAuthorizationStatusRestricted,
    CBPeripheralManagerAuthorizationStatusDenied,
    CBPeripheralManagerAuthorizationStatusAuthorized,       
};

typedef NS_ENUM(NSInteger, CBPeripheralManagerState) {
    CBPeripheralManagerStateUnknown = 0,
    CBPeripheralManagerStateResetting,
    CBPeripheralManagerStateUnsupported,
    CBPeripheralManagerStateUnauthorized,
    CBPeripheralManagerStatePoweredOff,
    CBPeripheralManagerStatePoweredOn,
};

typedef NS_ENUM(NSInteger, CBPeripheralManagerConnectionLatency) {
    CBPeripheralManagerConnectionLatencyLow = 0,
    CBPeripheralManagerConnectionLatencyMedium,
    CBPeripheralManagerConnectionLatencyHigh
};

@class CBCentral, CBService, CBMutableService, CBCharacteristic, CBMutableCharacteristic, CBATTRequest;
@protocol CBPeripheralManagerDelegate;

CB_EXTERN_CLASS @interface CBPeripheralManager : NSObject

@property(weak, nonatomic) id<CBPeripheralManagerDelegate> delegate;
@property(readonly) CBPeripheralManagerState state;
@property(readonly) BOOL isAdvertising;

+ (CBPeripheralManagerAuthorizationStatus)authorizationStatus;

- (instancetype)initWithDelegate:(id<CBPeripheralManagerDelegate>)delegate queue:(dispatch_queue_t)queue;
- (instancetype)initWithDelegate:(id<CBPeripheralManagerDelegate>)delegate queue:(dispatch_queue_t)queue options:(NSDictionary *)options;
- (void)startAdvertising:(NSDictionary *)advertisementData;
- (void)stopAdvertising;
- (void)setDesiredConnectionLatency:(CBPeripheralManagerConnectionLatency)latency forCentral:(CBCentral *)central;
- (void)addService:(CBMutableService *)service;
- (void)removeService:(CBMutableService *)service;
- (void)removeAllServices;
- (void)respondToRequest:(CBATTRequest *)request withResult:(CBATTError)result;
- (BOOL)updateValue:(NSData *)value forCharacteristic:(CBMutableCharacteristic *)characteristic onSubscribedCentrals:(NSArray *)centrals;

@end

@protocol CBPeripheralManagerDelegate <NSObject>

@required

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral;

@optional

- (void)peripheralManager:(CBPeripheralManager *)peripheral willRestoreState:(NSDictionary *)dict;
- (void)peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(NSError *)error;
- (void)peripheralManager:(CBPeripheralManager *)peripheral didAddService:(CBService *)service error:(NSError *)error;
- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didSubscribeToCharacteristic:(CBCharacteristic *)characteristic;
- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didUnsubscribeFromCharacteristic:(CBCharacteristic *)characteristic;
- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveReadRequest:(CBATTRequest *)request;
- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveWriteRequests:(NSArray *)requests;
- (void)peripheralManagerIsReadyToUpdateSubscribers:(CBPeripheralManager *)peripheral;

@end
