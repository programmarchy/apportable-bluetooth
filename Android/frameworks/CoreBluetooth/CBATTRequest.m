#import <CoreBluetooth/CBATTRequest.h>

@interface CBATTRequest ()

@property(readwrite, retain, nonatomic) CBCentral *central;
@property(readwrite, retain, nonatomic) CBCharacteristic *characteristic;
@property(readwrite, nonatomic) NSUInteger offset;

@end

@implementation CBATTRequest

@synthesize central;
@synthesize characteristic;
@synthesize offset;

@end
