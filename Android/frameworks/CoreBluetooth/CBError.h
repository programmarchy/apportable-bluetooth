#import <CoreBluetooth/CBDefines.h>
#import <Foundation/Foundation.h>

CB_EXTERN NSString * const CBErrorDomain;

typedef NS_ENUM(NSInteger, CBError) {
    CBErrorUnknown                                              = 0,
    CBErrorInvalidParameters                                    = 1,
    CBErrorInvalidHandle                                        = 2,
    CBErrorNotConnected                                         = 3,
    CBErrorOutOfSpace                                           = 4,
    CBErrorOperationCancelled                                   = 5,
    CBErrorConnectionTimeout                                    = 6,
    CBErrorPeripheralDisconnected                               = 7,
    CBErrorUUIDNotAllowed                                       = 8,
    CBErrorAlreadyAdvertising                                   = 9,
    CBErrorConnectionFailed                                     = 10
};

CB_EXTERN NSString * const CBATTErrorDomain;

typedef NS_ENUM(NSInteger, CBATTError) {
    CBATTErrorSuccess                               = 0x00,
    CBATTErrorInvalidHandle                         = 0x01,
    CBATTErrorReadNotPermitted                      = 0x02,
    CBATTErrorWriteNotPermitted                     = 0x03,
    CBATTErrorInvalidPdu                            = 0x04,
    CBATTErrorInsufficientAuthentication            = 0x05,
    CBATTErrorRequestNotSupported                   = 0x06,
    CBATTErrorInvalidOffset                         = 0x07,
    CBATTErrorInsufficientAuthorization             = 0x08,
    CBATTErrorPrepareQueueFull                      = 0x09,
    CBATTErrorAttributeNotFound                     = 0x0A,
    CBATTErrorAttributeNotLong                      = 0x0B,
    CBATTErrorInsufficientEncryptionKeySize         = 0x0C,
    CBATTErrorInvalidAttributeValueLength           = 0x0D,
    CBATTErrorUnlikelyError                         = 0x0E,
    CBATTErrorInsufficientEncryption                = 0x0F,
    CBATTErrorUnsupportedGroupType                  = 0x10,
    CBATTErrorInsufficientResources                 = 0x11
};
