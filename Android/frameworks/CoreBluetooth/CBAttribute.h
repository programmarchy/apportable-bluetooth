#import <CoreBluetooth/CBDefines.h>
#import <Foundation/Foundation.h>

@class CBUUID;

CB_EXTERN_CLASS @interface CBAttribute : NSObject

@property(readonly, nonatomic) CBUUID *UUID;

@end
