#import <CoreBluetooth/CBDefines.h>
#import <Foundation/Foundation.h>

CB_EXTERN NSString * const CBUUIDCharacteristicExtendedPropertiesString;
CB_EXTERN NSString * const CBUUIDCharacteristicUserDescriptionString;
CB_EXTERN NSString * const CBUUIDClientCharacteristicConfigurationString;
CB_EXTERN NSString * const CBUUIDServerCharacteristicConfigurationString;
CB_EXTERN NSString * const CBUUIDCharacteristicFormatString;
CB_EXTERN NSString * const CBUUIDCharacteristicAggregateFormatString;

CB_EXTERN NSString * const CBUUIDGenericAccessProfileString;
CB_EXTERN NSString * const CBUUIDGenericAttributeProfileString;
CB_EXTERN NSString * const CBUUIDDeviceNameString;
CB_EXTERN NSString * const CBUUIDAppearanceString;
CB_EXTERN NSString * const CBUUIDPeripheralPrivacyFlagString;
CB_EXTERN NSString * const CBUUIDReconnectionAddressString;
CB_EXTERN NSString * const CBUUIDPeripheralPreferredConnectionParametersString;
CB_EXTERN NSString * const CBUUIDServiceChangedString;

#ifdef APPORTABLE
@class AndroidUUID;
#endif

CB_EXTERN_CLASS @interface CBUUID : NSObject <NSCopying>

@property(nonatomic, readonly) NSData *data;
@property(nonatomic, readonly) NSString *UUIDString;

+ (CBUUID *)UUIDWithString:(NSString *)theString;
+ (CBUUID *)UUIDWithData:(NSData *)theData;
+ (CBUUID *)UUIDWithCFUUID:(CFUUIDRef)theUUID;
+ (CBUUID *)UUIDWithNSUUID:(NSUUID *)theUUID;

#ifdef APPORTABLE
+ (CBUUID *)UUIDWithAndroidUUID:(AndroidUUID *)theUUID;
#endif

@end
