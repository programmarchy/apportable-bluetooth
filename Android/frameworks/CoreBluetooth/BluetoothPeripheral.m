#import <CoreBluetooth/BluetoothPeripheral.h>
#import <BridgeKit/AndroidBluetoothGattService.h>
#import <BridgeKit/AndroidBluetoothGattCharacteristic.h>
#import <BridgeKit/AndroidBluetoothGattDescriptor.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation BluetoothPeripheral

@synthesize delegate;

+ (void)initializeJava
{
    [super initializeJava];

    [AndroidBluetoothGattService initializeJava];

    [BluetoothPeripheral
        registerConstructorWithSelector:@selector(initWithActivity:device:)
        arguments:[AndroidActivity className], [AndroidBluetoothDevice className], nil];

    [BluetoothPeripheral
        registerInstanceMethod:@"getName"
        selector:@selector(name)
        returnValue:[NSString className]
        arguments:nil];

    [BluetoothPeripheral
        registerInstanceMethod:@"getServices"
        selector:@selector(services)
        returnValue:[JavaList className]
        arguments:nil];

    [BluetoothPeripheral
        registerInstanceMethod:@"getState"
        selector:@selector(state)
        returnValue:[JavaClass intPrimitive]
        arguments:nil];

    [BluetoothPeripheral
        registerInstanceMethod:@"isConnected"
        selector:@selector(isConnected)
        returnValue:[JavaClass boolPrimitive]
        arguments:nil];

    [BluetoothPeripheral
        registerInstanceMethod:@"discoverServices"
        selector:@selector(discoverServices)];

    [BluetoothPeripheral
        registerInstanceMethod:@"readCharacteristic"
        selector:@selector(readCharacteristic:)
        arguments:[AndroidBluetoothGattCharacteristic className], nil];

    [BluetoothPeripheral
        registerInstanceMethod:@"writeCharacteristic"
        selector:@selector(writeCharacteristic:value:writeType:)
        arguments:[AndroidBluetoothGattCharacteristic className], [NSData className], [JavaClass intPrimitive], nil];

    [BluetoothPeripheral
        registerInstanceMethod:@"setCharacteristicNotification"
        selector:@selector(setCharacteristicNotification:enabled:)
        arguments:[AndroidBluetoothGattCharacteristic className], [JavaClass boolPrimitive], nil];

    [BluetoothPeripheral
        registerInstanceMethod:@"readDescriptor"
        selector:@selector(readDescriptor:)
        arguments:[AndroidBluetoothGattDescriptor className], nil];

    [BluetoothPeripheral
        registerInstanceMethod:@"writeDescriptor"
        selector:@selector(writeDescriptor:value:)
        arguments:[AndroidBluetoothGattDescriptor className], [NSData className], nil];

    [BluetoothPeripheral
        registerCallback:@"servicesDiscoveredCallback"
        selector:@selector(servicesDiscovered:)
        returnValue:nil
        arguments:[JavaClass intPrimitive], nil];

    [BluetoothPeripheral
        registerCallback:@"characteristicReadCallback"
        selector:@selector(characteristicRead:status:)
        returnValue:nil
        arguments:[AndroidBluetoothGattCharacteristic className], [JavaClass intPrimitive], nil];

    [BluetoothPeripheral
        registerCallback:@"characteristicWriteCallback"
        selector:@selector(characteristicWrite:status:)
        returnValue:nil
        arguments:[AndroidBluetoothGattCharacteristic className], [JavaClass intPrimitive], nil];

    [BluetoothPeripheral
        registerCallback:@"characteristicChangedCallback"
        selector:@selector(characteristicChanged:)
        returnValue:nil
        arguments:[AndroidBluetoothGattCharacteristic className], nil];

    [BluetoothPeripheral
        registerCallback:@"characteristicNotificationUpdatedCallback"
        selector:@selector(characteristicNotificationUpdated:status:)
        returnValue:nil
        arguments:[AndroidBluetoothGattCharacteristic className], [JavaClass intPrimitive], nil];

    [BluetoothPeripheral
        registerCallback:@"descriptorReadCallback"
        selector:@selector(descriptorRead:status:)
        returnValue:nil
        arguments:[AndroidBluetoothGattDescriptor className], [JavaClass intPrimitive], nil];

    [BluetoothPeripheral
        registerCallback:@"descriptorWriteCallback"
        selector:@selector(descriptorWrite:status:)
        returnValue:nil
        arguments:[AndroidBluetoothGattDescriptor className], [JavaClass intPrimitive], nil];
}

- (void)servicesDiscovered:(int)status
{
    if ([self.delegate respondsToSelector:@selector(bluetoothPeripheral:servicesDiscovered:)]) {
        [self.delegate bluetoothPeripheral:self servicesDiscovered:status];
    }
}

- (void)characteristicRead:(AndroidBluetoothGattCharacteristic *)gattCharacteristic status:(int)status
{
    if ([self.delegate respondsToSelector:@selector(bluetoothPeripheral:characteristicRead:status:)]) {
        [self.delegate bluetoothPeripheral:self characteristicRead:gattCharacteristic status:status];
    }
}

- (void)characteristicWrite:(AndroidBluetoothGattCharacteristic *)gattCharacteristic status:(int)status
{
    if ([self.delegate respondsToSelector:@selector(bluetoothPeripheral:characteristicWrite:status:)]) {
        [self.delegate bluetoothPeripheral:self characteristicWrite:gattCharacteristic status:status];
    }
}

- (void)characteristicChanged:(AndroidBluetoothGattCharacteristic *)gattCharacteristic
{
    if ([self.delegate respondsToSelector:@selector(bluetoothPeripheral:characteristicChanged:)]) {
        [self.delegate bluetoothPeripheral:self characteristicChanged:gattCharacteristic];
    }
}

- (void)characteristicNotificationUpdated:(AndroidBluetoothGattCharacteristic *)gattCharacteristic status:(int)status
{
    if ([self.delegate respondsToSelector:@selector(bluetoothPeripheral:characteristicNotificationUpdated:status:)]) {
        [self.delegate bluetoothPeripheral:self characteristicNotificationUpdated:gattCharacteristic status:status];
    }
}

- (void)descriptorRead:(AndroidBluetoothGattDescriptor *)gattDescriptor status:(int)status
{
    if ([self.delegate respondsToSelector:@selector(bluetoothPeripheral:descriptorRead:status:)]) {
        [self.delegate bluetoothPeripheral:self descriptorRead:gattDescriptor status:status];
    }
}

- (void)descriptorWrite:(AndroidBluetoothGattDescriptor *)gattDescriptor status:(int)status
{
    if ([self.delegate respondsToSelector:@selector(bluetoothPeripheral:descriptorWrite:status:)]) {
        [self.delegate bluetoothPeripheral:self descriptorWrite:gattDescriptor status:status];
    }
}

+ (NSString *)className
{
    return @"com.modrobotics.coreBluetooth.BluetoothPeripheral";
}

@end

#pragma clang diagnostic pop