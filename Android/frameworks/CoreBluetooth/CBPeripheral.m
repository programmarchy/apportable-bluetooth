#import <CoreBluetooth/CBPeripheral.h>
#import <CoreBluetooth/CBError.h>
#import <CoreBluetooth/CBService.h>
#import <CoreBluetooth/CBCharacteristic.h>
#import <CoreBluetooth/CBDescriptor.h>
#import <CoreBluetooth/BluetoothPeripheral.h>
#import <BridgeKit/AndroidActivity.h>
#import <BridgeKit/AndroidUUID.h>
#import <BridgeKit/AndroidBluetoothDevice.h>
#import <BridgeKit/AndroidBluetoothGatt.h>
#import <BridgeKit/AndroidBluetoothGattService.h>
#import <BridgeKit/AndroidBluetoothGattCharacteristic.h>
#import <BridgeKit/AndroidBluetoothGattDescriptor.h>

CBATTError CBATTErrorCodeForGattStatus(int status)
{
    switch (status) {
        case AndroidBluetoothGattInsufficientAuthentication:
            return CBATTErrorInsufficientAuthentication;
        case AndroidBluetoothGattInsufficientEncryption:
            return CBATTErrorInsufficientEncryption;
        case AndroidBluetoothGattInvalidAttributeLength:
            return CBATTErrorInvalidAttributeValueLength;
        case AndroidBluetoothGattInvalidOffset:
            return CBATTErrorInvalidOffset;
        case AndroidBluetoothGattReadNotPermitted:
            return CBATTErrorReadNotPermitted;
        case AndroidBluetoothGattRequestNotSupported:
            return CBATTErrorRequestNotSupported;
        case AndroidBluetoothGattWriteNotPermitted:
            return CBATTErrorWriteNotPermitted;
        default:
            return CBATTErrorUnlikelyError;
    }
}

int AndroidBluetoothGattCharacteristicWriteTypeForCBCharacteristicWriteType(CBCharacteristicWriteType type)
{
    switch (type) {
        case CBCharacteristicWriteWithoutResponse:
            return AndroidBluetoothGattCharacteristicWriteTypeNoResponse;
        case CBCharacteristicWriteWithResponse:
        default:
            return AndroidBluetoothGattCharacteristicWriteTypeDefault;
    }
}

@interface CBPeripheral () <BluetoothPeripheralDelegate>

@property(strong, nonatomic, readwrite) AndroidBluetoothDevice *bluetoothDevice;
@property(strong, nonatomic, readwrite) BluetoothPeripheral *bluetoothPeripheral;
@property(strong, nonatomic, readwrite) dispatch_queue_t queue;
@property(strong, nonatomic) NSMutableDictionary *serviceCache;

@end

@implementation CBPeripheral

- (id)initWithBluetoothDevice:(AndroidBluetoothDevice *)bluetoothDevice queue:(dispatch_queue_t)queue
{
    self = [super init];
    if (self) {
        self.bluetoothDevice = bluetoothDevice;
        self.bluetoothPeripheral = [[BluetoothPeripheral alloc] initWithActivity:[AndroidActivity currentActivity] device:bluetoothDevice];
        self.bluetoothPeripheral.delegate = self;
        self.queue = queue;
        self.serviceCache = [NSMutableDictionary dictionary];
    }
    return self;
}

- (NSString *)name
{
    return [self.bluetoothPeripheral name];
}

- (NSNumber *)RSSI
{
    return [NSNumber numberWithInt:0];
}

- (BOOL)isConnected
{
    return [self.bluetoothPeripheral isConnected];
}

- (CBPeripheralState)state
{
    // The state values are identical, so can be casted safely.
    return (CBPeripheralState)[self.bluetoothPeripheral state];
}

- (NSArray *)services
{
    NSMutableArray *services = [NSMutableArray array];
    NSArray *gattServices = [[self.bluetoothPeripheral services] toArray];
    for (AndroidBluetoothGattService *gattService in gattServices) {
        [services addObject:[self serviceForBluetoothGattService:gattService]];
    }
    return [NSArray arrayWithArray:services];
}

- (void)readRSSI
{
    CB_METHOD_NOT_IMPLEMENTED();
}

- (void)discoverServices:(NSArray *)serviceUUIDs
{
    [self.bluetoothPeripheral discoverServices];
}

- (void)discoverIncludedServices:(NSArray *)includedServiceUUIDs forService:(CBService *)service
{
    if ([self.delegate respondsToSelector:@selector(peripheral:didDiscoverIncludedServicesForService:error:)]) {
        dispatch_async(self.queue, ^{
            [self.delegate peripheral:self didDiscoverIncludedServicesForService:service error:nil];
        });
    }
}

- (void)discoverCharacteristics:(NSArray *)characteristicUUIDs forService:(CBService *)service
{
    if ([self.delegate respondsToSelector:@selector(peripheral:didDiscoverCharacteristicsForService:error:)]) {
        dispatch_async(self.queue, ^{
            [self.delegate peripheral:self didDiscoverCharacteristicsForService:service error:nil];
        });
    }
}

- (void)readValueForCharacteristic:(CBCharacteristic *)characteristic
{
    [self.bluetoothPeripheral readCharacteristic:characteristic.bluetoothGattCharacteristic];
}

- (void)writeValue:(NSData *)data forCharacteristic:(CBCharacteristic *)characteristic type:(CBCharacteristicWriteType)type
{
    [self.bluetoothPeripheral writeCharacteristic:characteristic.bluetoothGattCharacteristic value:data
        writeType:AndroidBluetoothGattCharacteristicWriteTypeForCBCharacteristicWriteType(type)];
}

- (void)setNotifyValue:(BOOL)enabled forCharacteristic:(CBCharacteristic *)characteristic
{
    [characteristic setPendingNotifyValue:enabled];
    [self.bluetoothPeripheral setCharacteristicNotification:characteristic.bluetoothGattCharacteristic enabled:enabled];
}

- (void)discoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic
{
    if ([self.delegate respondsToSelector:@selector(peripheral:didDiscoverDescriptorsForCharacteristic:error:)]) {
        dispatch_async(self.queue, ^{
            [self.delegate peripheral:self didDiscoverDescriptorsForCharacteristic:characteristic error:nil];
        });
    }
}

- (void)readValueForDescriptor:(CBDescriptor *)descriptor
{
    [self.bluetoothPeripheral readDescriptor:descriptor.bluetoothGattDescriptor];
}

- (void)writeValue:(NSData *)data forDescriptor:(CBDescriptor *)descriptor
{
    [self.bluetoothPeripheral writeDescriptor:descriptor.bluetoothGattDescriptor value:data];
}

#pragma mark - Helpers

- (CBService *)serviceForBluetoothGattService:(AndroidBluetoothGattService *)bluetoothGattService
{
    NSString *UUIDString = [bluetoothGattService.UUID toString];
    CBService *service = [self.serviceCache objectForKey:UUIDString];
    if (!service) {
        service = [[CBService alloc] initWithPeripheral:self bluetoothGattService:bluetoothGattService];
        [self.serviceCache setObject:service forKey:UUIDString];
    }
    return service;
}

- (CBCharacteristic *)characteristicForBluetoothGattCharacteristic:(AndroidBluetoothGattCharacteristic *)bluetoothGattCharacteristic
{
    CBService *service = [self serviceForBluetoothGattService:[bluetoothGattCharacteristic service]];
    return [service characteristicForBluetoothGattCharacteristic:bluetoothGattCharacteristic];
}

- (CBDescriptor *)descriptorForBluetoothGattDescriptor:(AndroidBluetoothGattDescriptor *)bluetoothGattDescriptor
{
    CBCharacteristic *characteristic = [self characteristicForBluetoothGattCharacteristic:[bluetoothGattDescriptor characteristic]];
    return [characteristic descriptorForBluetoothGattDescriptor:bluetoothGattDescriptor];
}

#pragma mark - BluetoothPeripheralDelegate

- (void)bluetoothPeripheral:(BluetoothPeripheral *)bluetoothPeripheral servicesDiscovered:(int)status
{
    if ([self.delegate respondsToSelector:@selector(peripheral:didDiscoverServices:)]) {
        dispatch_async(self.queue, ^{
            [self.delegate peripheral:self didDiscoverServices:
                (AndroidBluetoothGattSuccess == status) ? nil : [NSError
                    errorWithDomain:CBATTErrorDomain code:CBATTErrorCodeForGattStatus(status) userInfo:nil]];
        });
    }
}

- (void)bluetoothPeripheral:(BluetoothPeripheral *)bluetoothPeripheral characteristicRead:(AndroidBluetoothGattCharacteristic *)gattCharacteristic status:(int)status
{
    CBCharacteristic *characteristic = [self characteristicForBluetoothGattCharacteristic:gattCharacteristic];
    if ([self.delegate respondsToSelector:@selector(peripheral:didUpdateValueForCharacteristic:error:)]) {
        dispatch_async(self.queue, ^{
            [self.delegate peripheral:self didUpdateValueForCharacteristic:characteristic error:
                (AndroidBluetoothGattSuccess == status) ? nil : [NSError
                    errorWithDomain:CBATTErrorDomain code:CBATTErrorCodeForGattStatus(status) userInfo:nil]];
        });
    }
}

- (void)bluetoothPeripheral:(BluetoothPeripheral *)bluetoothPeripheral characteristicWrite:(AndroidBluetoothGattCharacteristic *)gattCharacteristic status:(int)status
{
    CBCharacteristic *characteristic = [self characteristicForBluetoothGattCharacteristic:gattCharacteristic];
    if ([self.delegate respondsToSelector:@selector(peripheral:didWriteValueForCharacteristic:error:)]) {
        dispatch_async(self.queue, ^{
            [self.delegate peripheral:self didWriteValueForCharacteristic:characteristic error:
                (AndroidBluetoothGattSuccess == status) ? nil : [NSError
                    errorWithDomain:CBATTErrorDomain code:CBATTErrorCodeForGattStatus(status) userInfo:nil]];
        });
    }
}

- (void)bluetoothPeripheral:(BluetoothPeripheral *)bluetoothPeripheral characteristicChanged:(AndroidBluetoothGattCharacteristic *)gattCharacteristic
{
    CBCharacteristic *characteristic = [self characteristicForBluetoothGattCharacteristic:gattCharacteristic];
    if ([self.delegate respondsToSelector:@selector(peripheral:didUpdateValueForCharacteristic:error:)]) {
        dispatch_async(self.queue, ^{
            [self.delegate peripheral:self didUpdateValueForCharacteristic:characteristic error:nil];
        });
    }
}

- (void)bluetoothPeripheral:(BluetoothPeripheral *)bluetoothPeripheral characteristicNotificationUpdated:(AndroidBluetoothGattCharacteristic *)gattCharacteristic status:(int)status
{
    CBCharacteristic *characteristic = [self characteristicForBluetoothGattCharacteristic:gattCharacteristic];
    if (AndroidBluetoothGattSuccess == status) {
        [characteristic confirmPendingNotifyValue];
    }
    if ([self.delegate respondsToSelector:@selector(peripheral:didUpdateNotificationStateForCharacteristic:error:)]) {
        dispatch_async(self.queue, ^{
            [self.delegate peripheral:self didUpdateNotificationStateForCharacteristic:characteristic error:
                (AndroidBluetoothGattSuccess == status) ? nil : [NSError
                    errorWithDomain:CBATTErrorDomain code:CBATTErrorCodeForGattStatus(status) userInfo:nil]];
        });
    }
}

- (void)bluetoothPeripheral:(BluetoothPeripheral *)bluetoothPeripheral descriptorRead:(AndroidBluetoothGattDescriptor *)gattDescriptor status:(int)status
{
    CBDescriptor *descriptor = [self descriptorForBluetoothGattDescriptor:gattDescriptor];
    if ([self.delegate respondsToSelector:@selector(peripheral:didUpdateValueForDescriptor:error:)]) {
        dispatch_async(self.queue, ^{
            [self.delegate peripheral:self didUpdateValueForDescriptor:descriptor error:
                (AndroidBluetoothGattSuccess == status) ? nil : [NSError
                    errorWithDomain:CBATTErrorDomain code:CBATTErrorCodeForGattStatus(status) userInfo:nil]];
        });
    }
}

- (void)bluetoothPeripheral:(BluetoothPeripheral *)bluetoothPeripheral descriptorWrite:(AndroidBluetoothGattDescriptor *)gattDescriptor status:(int)status
{
    CBDescriptor *descriptor = [self descriptorForBluetoothGattDescriptor:gattDescriptor];
    if ([self.delegate respondsToSelector:@selector(peripheral:didWriteValueForDescriptor:error:)]) {
        dispatch_async(self.queue, ^{
            [self.delegate peripheral:self didWriteValueForDescriptor:descriptor error:
                (AndroidBluetoothGattSuccess == status) ? nil : [NSError
                    errorWithDomain:CBATTErrorDomain code:CBATTErrorCodeForGattStatus(status) userInfo:nil]];
        });
    }
}

@end
