#import <BridgeKit/JavaObject.h>
#import <BridgeKit/AndroidActivity.h>

typedef enum {
    BluetoothCentralManagerStateUnknown = 0,
    BluetoothCentralManagerStateUnavailable = 1,
    BluetoothCentralManagerStateOff = 2,
    BluetoothCentralManagerStateOn = 3
} BluetoothCentralManagerState;

@class BluetoothCentralManager, BluetoothPeripheral, AndroidBluetoothDevice;

@protocol BluetoothCentralManagerDelegate <NSObject>

- (void)bluetoothCentralManagerStateChanged:(BluetoothCentralManager *)centralManager;
- (void)bluetoothCentralManager:(BluetoothCentralManager *)centralManager deviceDiscovered:(AndroidBluetoothDevice *)device RSSI:(int)RSSI scanRecord:(NSData *)scanRecord;
- (void)bluetoothCentralManager:(BluetoothCentralManager *)centralManager deviceConnected:(AndroidBluetoothDevice *)device;
- (void)bluetoothCentralManager:(BluetoothCentralManager *)centralManager deviceFailedToConnect:(AndroidBluetoothDevice *)device status:(int)status;
- (void)bluetoothCentralManager:(BluetoothCentralManager *)centralManager deviceDisconnected:(AndroidBluetoothDevice *)device status:(int)status;

@end

@interface BluetoothCentralManager : JavaObject

@property(weak, nonatomic) id<BluetoothCentralManagerDelegate> delegate;

- (id)initWithActivity:(AndroidActivity *)activity;
- (int)state;
- (void)updateState;
- (void)startScan;
- (void)stopScan;
- (void)connectPeripheral:(BluetoothPeripheral *)peripheral;
- (void)cancelPeripheralConnection:(BluetoothPeripheral *)peripheral;

@end
