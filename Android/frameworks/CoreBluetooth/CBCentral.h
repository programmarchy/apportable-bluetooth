#import <CoreBluetooth/CBDefines.h>
#import <CoreBluetooth/CBPeer.h>
#import <Foundation/Foundation.h>

@interface CBCentral : CBPeer

@property(readonly, nonatomic) NSUInteger maximumUpdateValueLength;

@end
