#ifndef CB_EXTERN
#ifdef __cplusplus
#define CB_EXTERN extern "C" __attribute__((visibility ("default")))
#else
#define CB_EXTERN extern __attribute__((visibility ("default")))
#endif
#endif

#define CB_EXTERN_CLASS __attribute__((visibility("default")))

#define CB_METHOD_NOT_IMPLEMENTED() @throw([NSException \
    exceptionWithName:NSInvalidArgumentException \
    reason:[NSString stringWithFormat:@"method not implemented: %@", NSStringFromSelector(_cmd)] \
    userInfo:nil])