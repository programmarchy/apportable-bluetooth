#import <CoreBluetooth/CBAttribute.h>

@class CBPeripheral, CBUUID;

#ifdef APPORTABLE
@class AndroidBluetoothGattService, CBCharacteristic, AndroidBluetoothGattCharacteristic;
#endif

CB_EXTERN_CLASS @interface CBService : CBAttribute

@property(weak, readonly, nonatomic) CBPeripheral *peripheral;
@property(readonly, nonatomic) BOOL isPrimary;
@property(retain, readonly) NSArray *includedServices;
@property(retain, readonly) NSArray *characteristics;

#ifdef APPORTABLE
@property(strong, readonly, nonatomic) AndroidBluetoothGattService *bluetoothGattService;

- (id)initWithPeripheral:(CBPeripheral *)peripheral bluetoothGattService:(AndroidBluetoothGattService *)bluetoothGattService;

- (CBCharacteristic *)characteristicForBluetoothGattCharacteristic:(AndroidBluetoothGattCharacteristic *)bluetoothGattCharacteristic;
#endif

@end

CB_EXTERN_CLASS @interface CBMutableService : CBService

@property(retain, readwrite, nonatomic) CBUUID *UUID;
@property(readwrite, nonatomic) BOOL isPrimary;
@property(retain, readwrite) NSArray *includedServices;
@property(retain, readwrite) NSArray *characteristics;

- (instancetype)initWithType:(CBUUID *)UUID primary:(BOOL)isPrimary;

@end
