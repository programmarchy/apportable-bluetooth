#import <CoreBluetooth/CBDefines.h>
#import <Foundation/Foundation.h>

CB_EXTERN NSString * const CBAdvertisementDataLocalNameKey;
CB_EXTERN NSString * const CBAdvertisementDataTxPowerLevelKey;
CB_EXTERN NSString * const CBAdvertisementDataServiceUUIDsKey;
CB_EXTERN NSString * const CBAdvertisementDataServiceDataKey;
CB_EXTERN NSString * const CBAdvertisementDataManufacturerDataKey;
CB_EXTERN NSString * const CBAdvertisementDataOverflowServiceUUIDsKey;
CB_EXTERN NSString * const CBAdvertisementDataIsConnectable;
CB_EXTERN NSString * const CBAdvertisementDataSolicitedServiceUUIDsKey;
