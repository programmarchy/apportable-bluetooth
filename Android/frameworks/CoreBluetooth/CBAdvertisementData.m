#import <CoreBluetooth/CBAdvertisementData.h>

NSString * const CBAdvertisementDataLocalNameKey = @"CBAdvertisementDataLocalNameKey";
NSString * const CBAdvertisementDataTxPowerLevelKey = @"CBAdvertisementDataTxPowerLevelKey";
NSString * const CBAdvertisementDataServiceUUIDsKey = @"CBAdvertisementDataServiceUUIDsKey";
NSString * const CBAdvertisementDataServiceDataKey = @"CBAdvertisementDataServiceDataKey";
NSString * const CBAdvertisementDataManufacturerDataKey = @"CBAdvertisementDataManufacturerDataKey";
NSString * const CBAdvertisementDataOverflowServiceUUIDsKey = @"CBAdvertisementDataOverflowServiceUUIDsKey";
NSString * const CBAdvertisementDataIsConnectable = @"CBAdvertisementDataIsConnectable";
NSString * const CBAdvertisementDataSolicitedServiceUUIDsKey = @"CBAdvertisementDataSolicitedServiceUUIDsKey";
