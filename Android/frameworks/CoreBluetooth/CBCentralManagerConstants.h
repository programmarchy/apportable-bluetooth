#import <CoreBluetooth/CBDefines.h>
#import <Foundation/Foundation.h>

CB_EXTERN NSString * const CBCentralManagerOptionShowPowerAlertKey;
CB_EXTERN NSString * const CBCentralManagerOptionRestoreIdentifierKey;
CB_EXTERN NSString * const CBCentralManagerScanOptionAllowDuplicatesKey;
CB_EXTERN NSString * const CBCentralManagerScanOptionSolicitedServiceUUIDsKey;
CB_EXTERN NSString * const CBConnectPeripheralOptionNotifyOnConnectionKey;
CB_EXTERN NSString * const CBConnectPeripheralOptionNotifyOnDisconnectionKey;
CB_EXTERN NSString * const CBConnectPeripheralOptionNotifyOnNotificationKey;
CB_EXTERN NSString * const CBCentralManagerRestoredStatePeripheralsKey;
CB_EXTERN NSString * const CBCentralManagerRestoredStateScanServicesKey;
CB_EXTERN NSString * const CBCentralManagerRestoredStateScanOptionsKey;
