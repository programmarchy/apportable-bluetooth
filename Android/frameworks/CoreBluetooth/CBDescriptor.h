#import <CoreBluetooth/CBAttribute.h>

@class CBCharacteristic, CBUUID;

#ifdef APPORTABLE
@class AndroidBluetoothGattDescriptor;
#endif

CB_EXTERN_CLASS @interface CBDescriptor : CBAttribute

@property(weak, readonly, nonatomic) CBCharacteristic *characteristic;
@property(retain, readonly) id value;

#ifdef APPORTABLE
@property(strong, readonly, nonatomic) AndroidBluetoothGattDescriptor *bluetoothGattDescriptor;

- (id)initWithCharacteristic:(CBCharacteristic *)characteristic bluetoothGattDescriptor:(AndroidBluetoothGattDescriptor *)bluetoothGattDescriptor;
#endif

@end

CB_EXTERN_CLASS @interface CBMutableDescriptor : CBDescriptor

- (instancetype)initWithType:(CBUUID *)UUID value:(id)value;

@end
