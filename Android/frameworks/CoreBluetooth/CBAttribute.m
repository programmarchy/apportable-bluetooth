#import <CoreBluetooth/CBAttribute.h>

@interface CBAttribute ()

@property(readwrite, nonatomic, strong) CBUUID *UUID;

@end

@implementation CBAttribute

@synthesize UUID;

@end
