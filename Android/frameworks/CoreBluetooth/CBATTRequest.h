#import <CoreBluetooth/CBDefines.h>
#import <Foundation/Foundation.h>

@class CBCentral, CBCharacteristic;

CB_EXTERN_CLASS @interface CBATTRequest : NSObject

@property(readonly, retain, nonatomic) CBCentral *central;
@property(readonly, retain, nonatomic) CBCharacteristic *characteristic;
@property(readonly, nonatomic) NSUInteger offset;
@property(readwrite, copy) NSData *value;

@end
