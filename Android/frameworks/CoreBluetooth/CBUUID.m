#import <CoreBluetooth/CBUUID.h>
#ifdef APPORTABLE
#import <BridgeKit/AndroidUUID.h>
#import <CoreFoundation/CFByteOrder.h>
#endif

@interface CBUUID () {
    NSString *_UUIDString;
}

@property(nonatomic, readwrite, strong) NSData *data;

@end

@implementation CBUUID

- (id)initWithData:(NSData *)theData
{
    if ([theData length] != 4 && [theData length] != 16) {
        [NSException raise:NSInternalInconsistencyException
            format:@"Data %@ does not represent a valid UUID", theData, nil];
    }
    self = [super init];
    if (self) {
        self.data = [NSData dataWithData:theData];
    }
    return self;
}

- (id)initWithString:(NSString *)theString
{
    NSPredicate *regEx = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",
        @"([0-9A-F]){8}-([0-9A-F]){4}-([0-9A-F]){4}-([0-9A-F]){4}-([0-9A-F]){12}"];
    theString = [theString uppercaseString];
    if ([regEx evaluateWithObject:theString] == NO) {
        [NSException raise:NSInternalInconsistencyException
            format:@"String %@ does not represent a valid UUID", theString, nil];
    }
    self = [super init];
    if (self) {
        self.data = [self dataFromUUIDString:theString];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] alloc] init];
    if (copy) {
        [copy setData:[self.data copyWithZone:zone]];
    }
    return copy;
}

- (NSUInteger)hash
{
    return 0; // Should never rely on hash. It's too small to represent a UUID.
}

- (BOOL)isEqual:(id)anObject
{
    if ([anObject isKindOfClass:[CBUUID class]]) {
        CBUUID *anUUID = (CBUUID *)anObject;
        return [self.data isEqualToData:anUUID.data];
    }
    return NO;
}

- (NSString *)hexStringFromData:(NSData *)theData
{
    NSUInteger length = [theData length];
    NSMutableString *string = [NSMutableString stringWithCapacity:length * 2];
    const unsigned char *bytes = [theData bytes];
    for (NSUInteger i = 0; i < length; ++i) {
        [string appendFormat:@"%02x", bytes[i]];
    }
    return string;
}

- (NSString *)UUIDStringFromData:(NSData *)theData
{
    if ([theData length] == 4) {
        return [self hexStringFromData:theData];
    }
    else if ([theData length] == 16) {
        return [NSString stringWithFormat:@"%@-%@-%@-%@-%@",
            [self hexStringFromData:[theData subdataWithRange:NSMakeRange(0, 4)]],
            [self hexStringFromData:[theData subdataWithRange:NSMakeRange(4, 2)]],
            [self hexStringFromData:[theData subdataWithRange:NSMakeRange(6, 2)]],
            [self hexStringFromData:[theData subdataWithRange:NSMakeRange(8, 2)]],
            [self hexStringFromData:[theData subdataWithRange:NSMakeRange(10, 6)]]];
    }
    else {
        @throw([NSException
            exceptionWithName:NSInternalInconsistencyException
            reason:@"Invalid UUID" userInfo:nil]);
    }
}

- (NSData *)dataFromHexString:(NSString *)hexString
{
    NSMutableData *mutableData = [[NSMutableData alloc] init];
    unsigned char hexByte;
    char hexByteChars[3] = {0x00, 0x00, 0x00};
    for (NSUInteger i = 0; i < ([hexString length] / 2); i++) {
        hexByteChars[0] = [hexString characterAtIndex:i * 2];
        hexByteChars[1] = [hexString characterAtIndex:i * 2 + 1];
        hexByte = strtol(hexByteChars, NULL, 16);
        [mutableData appendBytes:&hexByte length:1]; 
    }
    return [NSData dataWithData:mutableData];
}

- (NSData *)dataFromUUIDString:(NSString *)UUIDString
{
    return [self dataFromHexString:[UUIDString stringByReplacingOccurrencesOfString:@"-" withString:@""]];
}

- (NSString *)UUIDString
{
    if (nil == _UUIDString) {
        _UUIDString = [self UUIDStringFromData:self.data];
    }
    return _UUIDString;
}

+ (CBUUID *)UUIDWithString:(NSString *)theString
{
    return [[CBUUID alloc] initWithString:theString];
}

+ (CBUUID *)UUIDWithData:(NSData *)theData
{
    return [[CBUUID alloc] initWithData:theData];
}

+ (CBUUID *)UUIDWithCFUUID:(CFUUIDRef)theUUID
{
    CB_METHOD_NOT_IMPLEMENTED();
}

+ (CBUUID *)UUIDWithNSUUID:(NSUUID *)theUUID
{
    CB_METHOD_NOT_IMPLEMENTED();
}

#ifdef APPORTABLE
+ (CBUUID *)UUIDWithAndroidUUID:(AndroidUUID *)theUUID
{
    const uint64_t bytes[] = {
        CFSwapInt64([theUUID mostSignificantBits]),
        CFSwapInt64([theUUID leastSignificantBits])
    };
    return [CBUUID UUIDWithData:[NSData
        dataWithBytes:bytes length:sizeof(bytes)]];
}
#endif

@end
