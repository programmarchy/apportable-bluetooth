#import <CoreBluetooth/CBDefines.h>
#import <Foundation/Foundation.h>

CB_EXTERN_CLASS @interface CBPeer : NSObject <NSCopying>

@property(readonly, nonatomic) NSUUID *identifier;

@end
