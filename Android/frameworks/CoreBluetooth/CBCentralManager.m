#import <CoreBluetooth/CBCentralManager.h>
#import <CoreBluetooth/CBPeripheral.h>
#import <CoreBluetooth/CBError.h>
#import <CoreBluetooth/BluetoothCentralManager.h>
#import <CoreBluetooth/BluetoothPeripheral.h>
#import <BridgeKit/AndroidActivity.h>
#import <BridgeKit/AndroidBluetoothGatt.h>
#import <BridgeKit/AndroidBluetoothGattService.h>
#import <BridgeKit/AndroidBluetoothGattCharacteristic.h>
#import <BridgeKit/AndroidBluetoothGattDescriptor.h>

@interface CBCentralManager () <BluetoothCentralManagerDelegate> {
@private
    CBCentralManagerState _state;
}

@property(strong, nonatomic) dispatch_queue_t queue;
@property(strong, nonatomic) NSDictionary *options;
@property(strong, nonatomic) BluetoothCentralManager *bluetoothCentralManager;
@property(strong, nonatomic) NSMutableDictionary *peripheralCache;

@end

@implementation CBCentralManager

- (instancetype)initWithDelegate:(id<CBCentralManagerDelegate>)delegate queue:(dispatch_queue_t)queue
{
    return [self initWithDelegate:delegate queue:queue options:nil];
}

- (instancetype)initWithDelegate:(id<CBCentralManagerDelegate>)delegate queue:(dispatch_queue_t)queue options:(NSDictionary *)options
{
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.queue = nil == queue ? dispatch_get_main_queue() : queue;
        self.options = options;
        self.bluetoothCentralManager = [[BluetoothCentralManager alloc] initWithActivity:[AndroidActivity currentActivity]];
        self.bluetoothCentralManager.delegate = self;
        self.peripheralCache = [NSMutableDictionary dictionary];

        // Check if bluetooth is powered on
        [self.bluetoothCentralManager updateState];
    }
    return self;
}

- (void)setState:(CBCentralManagerState)state
{
    _state = state;
    dispatch_async(self.queue, ^{
        [self.delegate centralManagerDidUpdateState:self];
    });
}

- (CBCentralManagerState)state
{
    return _state;
}

- (void)scanForPeripheralsWithServices:(NSArray *)serviceUUIDs options:(NSDictionary *)options
{
    [self.bluetoothCentralManager startScan];
}

- (void)stopScan
{
    [self.bluetoothCentralManager stopScan];
}

- (void)connectPeripheral:(CBPeripheral *)peripheral options:(NSDictionary *)options
{
    [self.bluetoothCentralManager connectPeripheral:peripheral.bluetoothPeripheral];
}

- (void)cancelPeripheralConnection:(CBPeripheral *)peripheral
{
    [self.bluetoothCentralManager cancelPeripheralConnection:peripheral.bluetoothPeripheral];
}

- (void)retrievePeripherals:(NSArray *)peripheralUUIDs
{
    CB_METHOD_NOT_IMPLEMENTED();
}

- (NSArray *)retrievePeripheralsWithIdentifiers:(NSArray *)identifiers
{
    CB_METHOD_NOT_IMPLEMENTED();
}

- (void)retrieveConnectedPeripherals
{
    CB_METHOD_NOT_IMPLEMENTED();
}

- (NSArray *)retrieveConnectedPeripheralsWithServices:(NSArray *)serviceUUIDs
{
    CB_METHOD_NOT_IMPLEMENTED();
}

#pragma mark - Helpers

- (CBPeripheral *)peripheralForBluetoothDevice:(AndroidBluetoothDevice *)bluetoothDevice
{
    NSString *addressString = [bluetoothDevice address];
    CBPeripheral *peripheral = [self.peripheralCache objectForKey:addressString];
    if (!peripheral) {
        peripheral = [[CBPeripheral alloc] initWithBluetoothDevice:bluetoothDevice queue:self.queue];
        [self.peripheralCache setObject:peripheral forKey:addressString];
    }
    return peripheral;
}

- (NSDictionary *)advertisementDataForScanRecord:(NSData *)scanRecord
{
    return @{};
}

#pragma mark - BluetoothCentralManagerDelegate

- (void)bluetoothCentralManagerStateChanged:(BluetoothCentralManager *)bluetoothCentralManager
{
    switch ((BluetoothCentralManagerState)[bluetoothCentralManager state]) {
        case BluetoothCentralManagerStateUnavailable:
            self.state = CBCentralManagerStateUnsupported;
            break;
        case BluetoothCentralManagerStateOff:
            self.state = CBCentralManagerStatePoweredOff;
            break;
        case BluetoothCentralManagerStateOn:
            self.state = CBCentralManagerStatePoweredOn;
            break;
        default:
            self.state = CBCentralManagerStateUnknown;
            break;
    }
}

- (void)bluetoothCentralManager:(BluetoothCentralManager *)bluetoothCentralManager deviceDiscovered:(AndroidBluetoothDevice *)bluetoothDevice RSSI:(int)RSSI scanRecord:(NSData *)scanRecord
{
    CBPeripheral *peripheral = [self peripheralForBluetoothDevice:bluetoothDevice];
    NSDictionary *advertisementData = [self advertisementDataForScanRecord:scanRecord];
    NSNumber *RSSINumber = [NSNumber numberWithInt:RSSI];
    if ([self.delegate respondsToSelector:@selector(centralManager:didDiscoverPeripheral:advertisementData:RSSI:)]) {
        dispatch_async(self.queue, ^{
            [self.delegate centralManager:self didDiscoverPeripheral:peripheral advertisementData:advertisementData RSSI:RSSINumber];
        });
    }
}

- (void)bluetoothCentralManager:(BluetoothCentralManager *)bluetoothCentralManager deviceConnected:(AndroidBluetoothDevice *)bluetoothDevice
{
    CBPeripheral *peripheral = [self peripheralForBluetoothDevice:bluetoothDevice];
    if ([self.delegate respondsToSelector:@selector(centralManager:didConnectPeripheral:)]) {
        dispatch_async(self.queue, ^{
            [self.delegate centralManager:self didConnectPeripheral:peripheral];
        });
    }
}

- (void)bluetoothCentralManager:(BluetoothCentralManager *)bluetoothCentralManager deviceFailedToConnect:(AndroidBluetoothDevice *)bluetoothDevice status:(int)status
{
    CBPeripheral *peripheral = [self peripheralForBluetoothDevice:bluetoothDevice];
    if ([self.delegate respondsToSelector:@selector(centralManager:didFailToConnectPeripheral:error:)]) {
        dispatch_async(self.queue, ^{
            [self.delegate centralManager:self didFailToConnectPeripheral:peripheral error:[NSError
                errorWithDomain:CBErrorDomain code:CBErrorConnectionFailed userInfo:nil]];
        });
    }
}

- (void)bluetoothCentralManager:(BluetoothCentralManager *)bluetoothCentralManager deviceDisconnected:(AndroidBluetoothDevice *)bluetoothDevice status:(int)status
{
    CBPeripheral *peripheral = [self peripheralForBluetoothDevice:bluetoothDevice];
    if ([self.delegate respondsToSelector:@selector(centralManager:didDisconnectPeripheral:error:)]) {
        dispatch_async(self.queue, ^{
            [self.delegate centralManager:self didDisconnectPeripheral:peripheral error:
                (AndroidBluetoothGattSuccess == status) ? nil : [NSError
                    errorWithDomain:CBErrorDomain code:CBErrorPeripheralDisconnected userInfo:nil]];
        });
    }
}

@end
