#import <CoreBluetooth/CBService.h>
#import <CoreBluetooth/CBUUID.h>
#import <CoreBluetooth/CBCharacteristic.h>
#import <BridgeKit/AndroidUUID.h>
#import <BridgeKit/AndroidBluetoothGattService.h>
#import <BridgeKit/AndroidBluetoothGattCharacteristic.h>

@interface CBService () {
    CBUUID *_UUID;
}

@property(weak, readwrite, nonatomic) CBPeripheral *peripheral;
@property(strong, readwrite, nonatomic) AndroidBluetoothGattService *bluetoothGattService;
@property(strong, nonatomic) NSMutableDictionary *characteristicCache;

@end

@implementation CBService

@dynamic UUID;

- (id)initWithPeripheral:(CBPeripheral *)peripheral bluetoothGattService:(AndroidBluetoothGattService *)bluetoothGattService
{
    self = [super init];
    if (self) {
        self.peripheral = peripheral;
        self.bluetoothGattService = bluetoothGattService;
        self.characteristicCache = [NSMutableDictionary dictionary];
    }
    return self;
}

- (BOOL)isPrimary
{
    CB_METHOD_NOT_IMPLEMENTED();
}

- (NSArray *)includedServices
{
    CB_METHOD_NOT_IMPLEMENTED();
}

- (CBCharacteristic *)characteristicForBluetoothGattCharacteristic:(AndroidBluetoothGattCharacteristic *)bluetoothGattCharacteristic
{
    NSString *UUIDString = [bluetoothGattCharacteristic.UUID toString];
    CBCharacteristic *characteristic = [self.characteristicCache objectForKey:UUIDString];
    if (!characteristic) {
        characteristic = [[CBCharacteristic alloc] initWithService:self bluetoothGattCharacteristic:bluetoothGattCharacteristic];
        [self.characteristicCache setObject:characteristic forKey:UUIDString];
    }
    return characteristic;
}

- (NSArray *)characteristics
{
    NSMutableArray *characteristics = [NSMutableArray array];
    NSArray *gattCharacteristics = [[self.bluetoothGattService characteristics] toArray];
    for (AndroidBluetoothGattCharacteristic *gattCharacteristic in gattCharacteristics) {
        [characteristics addObject:[self characteristicForBluetoothGattCharacteristic:gattCharacteristic]];
    }
    return [NSArray arrayWithArray:characteristics];
}

- (CBUUID *)UUID
{
    if (_UUID == nil) {
        _UUID = [CBUUID UUIDWithAndroidUUID:[self.bluetoothGattService UUID]];
    }
    return _UUID;
}

@end

@implementation CBMutableService

@synthesize isPrimary;
@synthesize includedServices;
@synthesize characteristics;

@dynamic UUID;

- (instancetype)initWithType:(CBUUID *)UUID primary:(BOOL)isPrimary
{
    CB_METHOD_NOT_IMPLEMENTED();
}

@end
