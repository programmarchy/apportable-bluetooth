#import <CoreBluetooth/CBDescriptor.h>
#import <CoreBluetooth/CBUUID.h>
#import <BridgeKit/AndroidUUID.h>
#import <BridgeKit/AndroidBluetoothGattDescriptor.h>

@interface CBDescriptor () {
    CBUUID *_UUID;
}

@property(weak, readwrite, nonatomic) CBCharacteristic *characteristic;
@property(strong, readwrite, nonatomic) AndroidBluetoothGattDescriptor *bluetoothGattDescriptor;

@end

@implementation CBDescriptor

- (id)initWithCharacteristic:(CBCharacteristic *)characteristic bluetoothGattDescriptor:(AndroidBluetoothGattDescriptor *)bluetoothGattDescriptor
{
    self = [super init];
    if (self) {
        self.characteristic = characteristic;
        self.bluetoothGattDescriptor = bluetoothGattDescriptor;
    }
    return self;
}

- (id)value
{
    return [self.bluetoothGattDescriptor value];
}

- (CBUUID *)UUID
{
    if (_UUID == nil) {
        _UUID = [CBUUID UUIDWithAndroidUUID:[self.bluetoothGattDescriptor UUID]];
    }
    return _UUID;
}

@end
