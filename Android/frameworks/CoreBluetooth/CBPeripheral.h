#import <CoreBluetooth/CBDefines.h>
#import <CoreBluetooth/CBPeer.h>
#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, CBPeripheralState) {
    CBPeripheralStateDisconnected = 0,
    CBPeripheralStateConnecting,
    CBPeripheralStateConnected,
};

typedef NS_ENUM(NSInteger, CBCharacteristicWriteType) {
    CBCharacteristicWriteWithResponse = 0,
    CBCharacteristicWriteWithoutResponse,
};

@protocol CBPeripheralDelegate;
@class CBService, CBCharacteristic, CBDescriptor, CBUUID;

#ifdef APPORTABLE
@class BluetoothPeripheral, AndroidBluetoothDevice, AndroidBluetoothGatt, AndroidBluetoothGattService, AndroidBluetoothGattCharacteristic, AndroidBluetoothGattDescriptor;
#endif

CB_EXTERN_CLASS @interface CBPeripheral : CBPeer

@property(weak, nonatomic) id<CBPeripheralDelegate> delegate;
@property(retain, readonly) NSString *name;
@property(retain, readonly) NSNumber *RSSI;
@property(readonly) BOOL isConnected;
@property(readonly) CBPeripheralState state;
@property(retain, readonly) NSArray *services;

- (void)readRSSI;
- (void)discoverServices:(NSArray *)serviceUUIDs;
- (void)discoverIncludedServices:(NSArray *)includedServiceUUIDs forService:(CBService *)service;
- (void)discoverCharacteristics:(NSArray *)characteristicUUIDs forService:(CBService *)service;
- (void)readValueForCharacteristic:(CBCharacteristic *)characteristic;
- (void)writeValue:(NSData *)data forCharacteristic:(CBCharacteristic *)characteristic type:(CBCharacteristicWriteType)type;
- (void)setNotifyValue:(BOOL)enabled forCharacteristic:(CBCharacteristic *)characteristic;
- (void)discoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic;
- (void)readValueForDescriptor:(CBDescriptor *)descriptor;
- (void)writeValue:(NSData *)data forDescriptor:(CBDescriptor *)descriptor;

#ifdef APPORTABLE
@property(strong, nonatomic, readonly) AndroidBluetoothDevice *bluetoothDevice;
@property(strong, nonatomic, readonly) BluetoothPeripheral *bluetoothPeripheral;

- (id)initWithBluetoothDevice:(AndroidBluetoothDevice *)bluetoothDevice queue:(dispatch_queue_t)queue;

- (CBService *)serviceForBluetoothGattService:(AndroidBluetoothGattService *)bluetoothGattService;
- (CBCharacteristic *)characteristicForBluetoothGattCharacteristic:(AndroidBluetoothGattCharacteristic *)bluetoothGattCharacteristic;
- (CBDescriptor *)descriptorForBluetoothGattDescriptor:(AndroidBluetoothGattDescriptor *)bluetoothGattDescriptor;
#endif

@end

@protocol CBPeripheralDelegate <NSObject>

@optional

- (void)peripheralDidUpdateName:(CBPeripheral *)peripheral;
- (void)peripheralDidInvalidateServices:(CBPeripheral *)peripheral;
- (void)peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray *)invalidatedServices;
- (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error;
- (void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(NSError *)error;
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error;
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error;
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error;
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error;
- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error;
- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error;
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error;
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error;
- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error;

@end
