#import <CoreBluetooth/CBCharacteristic.h>
#import <CoreBluetooth/CBUUID.h>
#import <CoreBluetooth/CBDescriptor.h>
#import <BridgeKit/AndroidUUID.h>
#import <BridgeKit/AndroidBluetoothGattCharacteristic.h>
#import <BridgeKit/AndroidBluetoothGattDescriptor.h>

@interface CBCharacteristic () {
    CBUUID *_UUID;
    BOOL _pendingNotifyValue;
    BOOL _isNotifying;
}

@property(weak, readwrite, nonatomic) CBService *service;
@property(strong, readwrite, nonatomic) AndroidBluetoothGattCharacteristic *bluetoothGattCharacteristic;
@property(strong, nonatomic) NSMutableDictionary *descriptorCache;

@end

@implementation CBCharacteristic

- (id)initWithService:(CBService *)service bluetoothGattCharacteristic:(AndroidBluetoothGattCharacteristic *)bluetoothGattCharacteristic
{
    self = [super init];
    if (self) {
        self.service = service;
        self.bluetoothGattCharacteristic = bluetoothGattCharacteristic;
        self.descriptorCache = [NSMutableDictionary dictionary];
    }
    return self;
}

- (CBCharacteristicProperties)properties
{
    CBCharacteristicProperties properties = 0x0;
    int flags = [self.bluetoothGattCharacteristic properties];
    if (AndroidBluetoothGattCharacteristicPropertyBroadcast & flags) {
        properties |= CBCharacteristicPropertyBroadcast;
    }
    if (AndroidBluetoothGattCharacteristicPropertyIndicate & flags) {
        properties |= CBCharacteristicPropertyIndicate;
    }
    if (AndroidBluetoothGattCharacteristicPropertyNotify & flags) {
        properties |= CBCharacteristicPropertyNotify;
    }
    if (AndroidBluetoothGattCharacteristicPropertyRead & flags) {
        properties |= CBCharacteristicPropertyRead;
    }
    if (AndroidBluetoothGattCharacteristicPropertyWrite & flags) {
        properties |= CBCharacteristicPropertyWrite;
    }
    if (AndroidBluetoothGattCharacteristicPropertyWriteNoResponse & flags) {
        properties |= CBCharacteristicPropertyWriteWithoutResponse;
    }
    return properties;
}

- (NSData *)value
{
    return [self.bluetoothGattCharacteristic value];
}

- (CBDescriptor *)descriptorForBluetoothGattDescriptor:(AndroidBluetoothGattDescriptor *)bluetoothGattDescriptor
{
    NSString *UUIDString = [bluetoothGattDescriptor.UUID toString];
    CBDescriptor *descriptor = [self.descriptorCache objectForKey:UUIDString];
    if (!descriptor) {
        descriptor = [[CBDescriptor alloc] initWithCharacteristic:self bluetoothGattDescriptor:bluetoothGattDescriptor];
        [self.descriptorCache setObject:descriptor forKey:UUIDString];
    }
    return descriptor;
}

- (NSArray *)descriptors
{
    NSMutableArray *descriptors = [NSMutableArray array];
    NSArray *gattDescriptors = [[self.bluetoothGattCharacteristic descriptors] toArray];
    for (AndroidBluetoothGattDescriptor *gattDescriptor in gattDescriptors) {
        [descriptors addObject:[self descriptorForBluetoothGattDescriptor:gattDescriptor]];
    }
    return [NSArray arrayWithArray:descriptors];
}

- (BOOL)isBroadcasted
{
    // This property should not be used according to Apple documentation.
    return NO;
}

- (BOOL)isNotifying
{
    return _isNotifying;
}

- (CBUUID *)UUID
{
    if (_UUID == nil) {
        _UUID = [CBUUID UUIDWithAndroidUUID:[self.bluetoothGattCharacteristic UUID]];
    }
    return _UUID;
}

- (void)setPendingNotifyValue:(BOOL)isNotifying
{
    _pendingNotifyValue = isNotifying;
}

- (void)confirmPendingNotifyValue
{
    _isNotifying = _pendingNotifyValue;
}

@end
