#import <CoreBluetooth/CBPeer.h>

@interface CBPeer () {
    NSUUID *_identifier;
}

@end

@implementation CBPeer

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] alloc] init];
    if (copy) {
        [copy setIdentifier:[self.identifier copyWithZone:zone]];
    }
    return copy;
}

- (NSUUID *)identifier
{
    if (_identifier == nil) {
        _identifier = [NSUUID UUID];
    }
    return _identifier;
}

- (void)setIdentifier:(NSUUID *)identifier
{
    _identifier = identifier;
}

@end
