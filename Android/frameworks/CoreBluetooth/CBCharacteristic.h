#import <CoreBluetooth/CBAttribute.h>

typedef NS_OPTIONS(NSUInteger, CBCharacteristicProperties) {
    CBCharacteristicPropertyBroadcast                                               = 0x01,
    CBCharacteristicPropertyRead                                                    = 0x02,
    CBCharacteristicPropertyWriteWithoutResponse                                    = 0x04,
    CBCharacteristicPropertyWrite                                                   = 0x08,
    CBCharacteristicPropertyNotify                                                  = 0x10,
    CBCharacteristicPropertyIndicate                                                = 0x20,
    CBCharacteristicPropertyAuthenticatedSignedWrites                               = 0x40,
    CBCharacteristicPropertyExtendedProperties                                      = 0x80,
    CBCharacteristicPropertyNotifyEncryptionRequired                                = 0x100,
    CBCharacteristicPropertyIndicateEncryptionRequired                              = 0x200
};

@class CBService, CBUUID;

#ifdef APPORTABLE
@class AndroidBluetoothGattCharacteristic, CBDescriptor, AndroidBluetoothGattDescriptor;
#endif

CB_EXTERN_CLASS @interface CBCharacteristic : CBAttribute

@property(weak, readonly, nonatomic) CBService *service;
@property(readonly, nonatomic) CBCharacteristicProperties properties;
@property(retain, readonly) NSData *value;
@property(retain, readonly) NSArray *descriptors;
@property(readonly) BOOL isBroadcasted;
@property(readonly) BOOL isNotifying;

#ifdef APPORTABLE
@property(strong, readonly, nonatomic) AndroidBluetoothGattCharacteristic *bluetoothGattCharacteristic;

- (id)initWithService:(CBService *)service bluetoothGattCharacteristic:(AndroidBluetoothGattCharacteristic *)bluetoothGattCharacteristic;

- (CBDescriptor *)descriptorForBluetoothGattDescriptor:(AndroidBluetoothGattDescriptor *)bluetoothGattDescriptor;
- (void)setPendingNotifyValue:(BOOL)isNotifying;
- (void)confirmPendingNotifyValue;
#endif

@end

typedef NS_OPTIONS(NSUInteger, CBAttributePermissions) {
    CBAttributePermissionsReadable                  = 0x01,
    CBAttributePermissionsWriteable                 = 0x02,
    CBAttributePermissionsReadEncryptionRequired    = 0x04,
    CBAttributePermissionsWriteEncryptionRequired   = 0x08
};

CB_EXTERN_CLASS @interface CBMutableCharacteristic : CBCharacteristic

@property(assign, readwrite, nonatomic) CBAttributePermissions permissions;
@property(retain, readonly) NSArray *subscribedCentrals;
@property(retain, readwrite, nonatomic) CBUUID *UUID;
@property(assign, readwrite, nonatomic) CBCharacteristicProperties properties;
@property(retain, readwrite) NSData *value;
@property(retain, readwrite) NSArray *descriptors;

- (instancetype)initWithType:(CBUUID *)UUID properties:(CBCharacteristicProperties)properties value:(NSData *)value permissions:(CBAttributePermissions)permissions;

@end
