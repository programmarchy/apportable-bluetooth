#import <CoreBluetooth/CBCentralManagerConstants.h>

NSString * const CBCentralManagerOptionShowPowerAlertKey = @"CBCentralManagerOptionShowPowerAlertKey";
NSString * const CBCentralManagerOptionRestoreIdentifierKey = @"CBCentralManagerOptionRestoreIdentifierKey";
NSString * const CBCentralManagerScanOptionAllowDuplicatesKey = @"CBCentralManagerScanOptionAllowDuplicatesKey";
NSString * const CBCentralManagerScanOptionSolicitedServiceUUIDsKey = @"CBCentralManagerScanOptionSolicitedServiceUUIDsKey";
NSString * const CBConnectPeripheralOptionNotifyOnConnectionKey = @"CBConnectPeripheralOptionNotifyOnConnectionKey";
NSString * const CBConnectPeripheralOptionNotifyOnDisconnectionKey = @"CBConnectPeripheralOptionNotifyOnDisconnectionKey";
NSString * const CBConnectPeripheralOptionNotifyOnNotificationKey = @"CBConnectPeripheralOptionNotifyOnNotificationKey";
NSString * const CBCentralManagerRestoredStatePeripheralsKey = @"CBCentralManagerRestoredStatePeripheralsKey";
NSString * const CBCentralManagerRestoredStateScanServicesKey = @"CBCentralManagerRestoredStateScanServicesKey";
NSString * const CBCentralManagerRestoredStateScanOptionsKey = @"CBCentralManagerRestoredStateScanOptionsKey";
