#import <BridgeKit/JavaObject.h>
#import <BridgeKit/JavaList.h>
#import <BridgeKit/AndroidActivity.h>
#import <BridgeKit/AndroidBluetoothDevice.h>

typedef enum {
    BluetoothPeripheralStateDisconnected,
    BluetoothPeripheralStateConnecting,
    BluetoothPeripheralStateConnected
} BluetoothPeripheralState;

@class BluetoothPeripheral, AndroidBluetoothGattCharacteristic, AndroidBluetoothGattDescriptor;

@protocol BluetoothPeripheralDelegate <NSObject>

- (void)bluetoothPeripheral:(BluetoothPeripheral *)peripheral servicesDiscovered:(int)status;
- (void)bluetoothPeripheral:(BluetoothPeripheral *)peripheral characteristicRead:(AndroidBluetoothGattCharacteristic *)characteristic status:(int)status;
- (void)bluetoothPeripheral:(BluetoothPeripheral *)peripheral characteristicWrite:(AndroidBluetoothGattCharacteristic *)characteristic status:(int)status;
- (void)bluetoothPeripheral:(BluetoothPeripheral *)peripheral characteristicChanged:(AndroidBluetoothGattCharacteristic *)characteristic;
- (void)bluetoothPeripheral:(BluetoothPeripheral *)peripheral characteristicNotificationUpdated:(AndroidBluetoothGattCharacteristic *)characteristic status:(int)status;
- (void)bluetoothPeripheral:(BluetoothPeripheral *)peripheral descriptorRead:(AndroidBluetoothGattDescriptor *)descriptor status:(int)status;
- (void)bluetoothPeripheral:(BluetoothPeripheral *)peripheral descriptorWrite:(AndroidBluetoothGattDescriptor *)descriptor status:(int)status;

@end

@interface BluetoothPeripheral : JavaObject

@property(weak, nonatomic) id<BluetoothPeripheralDelegate> delegate;

- (id)initWithActivity:(AndroidActivity *)activity device:(AndroidBluetoothDevice *)device;
- (NSString *)name;
- (JavaList *)services;
- (int)state;
- (BOOL)isConnected;
- (void)discoverServices;
- (void)readCharacteristic:(AndroidBluetoothGattCharacteristic *)characteristic;
- (void)writeCharacteristic:(AndroidBluetoothGattCharacteristic *)characteristic value:(NSData *)value writeType:(int)writeType;
- (void)setCharacteristicNotification:(AndroidBluetoothGattCharacteristic *)characteristic enabled:(BOOL)enabled;
- (void)readDescriptor:(AndroidBluetoothGattDescriptor *)descriptor;
- (void)writeDescriptor:(AndroidBluetoothGattDescriptor *)descriptor value:(NSData *)value;

@end
