#import <CoreBluetooth/CBDefines.h>
#import <Foundation/Foundation.h>

CB_EXTERN NSString * const CBPeripheralManagerOptionShowPowerAlertKey;
CB_EXTERN NSString * const CBPeripheralManagerOptionRestoreIdentifierKey;
CB_EXTERN NSString * const CBPeripheralManagerRestoredStateServicesKey;
CB_EXTERN NSString * const CBPeripheralManagerRestoredStateAdvertisementDataKey;
