#import <Foundation/Foundation.h>

@class EAAccessory;

@interface EASession : NSObject

@property(nonatomic, readonly) EAAccessory *accessory;
@property(nonatomic, readonly) NSString *protocolString;
@property(nonatomic, readonly) NSInputStream *inputStream;
@property(nonatomic, readonly) NSOutputStream *outputStream;

- (id)initWithAccessory:(EAAccessory *)accessory forProtocol:(NSString *)protocolString;

@end
