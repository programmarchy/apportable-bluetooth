#import <ExternalAccessory/EAAccessoryManager.h>
#import <ExternalAccessory/EAAccessory.h>
#import <BridgeKit/AndroidBluetoothAdapter.h>
#import <BridgeKit/AndroidBluetoothDevice.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

NSString * const EABluetoothAccessoryPickerErrorDomain = @"EABluetoothAccessoryPickerErrorDomain";

NSString * const EAAccessoryDidConnectNotification = @"EAAccessoryDidConnectNotification";
NSString * const EAAccessoryDidDisconnectNotification = @"EAAccessoryDidDisconnectNotification";
NSString * const EAAccessoryKey = @"EAAccessoryKey";
NSString * const EAAccessorySelectedKey = @"EAAccessorySelectedKey";

@interface EAAccessoryManager ()

@property(nonatomic, assign, getter=isRegisteredForLocalNotifications) BOOL registeredForLocalNotifications;

@end

@implementation EAAccessoryManager

+ (EAAccessoryManager *)sharedAccessoryManager
{
    static dispatch_once_t onceToken;
    static id sharedInstance;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[EAAccessoryManager alloc] init];
    });
    return sharedInstance;
}

- (NSArray *)connectedAccessories
{
    NSMutableArray *connectedAccessories = [[NSMutableArray alloc] init];
    AndroidBluetoothAdapter *bluetoothAdapter = [AndroidBluetoothAdapter defaultAdapter];
    NSArray *bondedDevices = [[bluetoothAdapter bondedDevices] toArray];
    for (AndroidBluetoothDevice *device in bondedDevices) {
        if ([device.name hasPrefix:@"Moss"]) {
            [connectedAccessories addObject:[[EAAccessory alloc] initWithBluetoothDevice:device manager:self]];
        }
    }
    return connectedAccessories;
}

- (void)showBluetoothAccessoryPickerWithNameFilter:(NSPredicate *)predicate completion:(EABluetoothAccessoryPickerCompletion)completion
{
    EA_METHOD_NOT_IMPLEMENTED();
}

- (BOOL)isRegisteredForLocalNotifications
{
    // Disable notification registering. Since the accessory list is
    // built from bonded devices, we don't ever want to remove them
    // from the list.
    return NO;
}

- (void)registerForLocalNotifications
{
    self.registeredForLocalNotifications = YES;
}

- (void)unregisterForLocalNotifications
{
    self.registeredForLocalNotifications = NO;
}

@end

#pragma clang diagnostic pop