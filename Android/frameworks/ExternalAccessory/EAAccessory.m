#import <ExternalAccessory/EAAccessory.h>
#import <BridgeKit/AndroidBluetoothDevice.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

@interface EAAccessory ()

@property(nonatomic, readwrite, strong) AndroidBluetoothDevice *bluetoothDevice;
@property(nonatomic, readwrite, strong) EAAccessoryManager *manager;

@end

@implementation EAAccessory

- (id)init
{
    return [super init];
}

- (id)initWithBluetoothDevice:(AndroidBluetoothDevice *)bluetoothDevice manager:(EAAccessoryManager *)manager
{
    self = [super init];
    if (self) {
        self.bluetoothDevice = bluetoothDevice;
        self.manager = manager;
    }
    return self;
}

- (NSString *)name
{
    return [self.bluetoothDevice name];
}

- (BOOL)isConnected
{
    return YES;
}

- (NSUInteger)connectionID
{
    return (NSUInteger)[self.bluetoothDevice hashCode];
}

- (NSArray *)protocolStrings
{
    return @[ @"com.modrobotics.moss" ];
}

- (BOOL)isEqual:(id)otherObject
{
    if ([otherObject isKindOfClass:[EAAccessory class]]) {
        return self.connectionID == ((EAAccessory *)otherObject).connectionID;
    }
    return NO;
}

@end

#pragma clang diagnostic pop