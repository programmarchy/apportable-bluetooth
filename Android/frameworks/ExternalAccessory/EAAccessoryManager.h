#import <Foundation/Foundation.h>
#import <ExternalAccessory/ExternalAccessoryDefines.h>

EA_EXTERN NSString *const EABluetoothAccessoryPickerErrorDomain;

EA_EXTERN NSString *const EAAccessoryDidConnectNotification;
EA_EXTERN NSString *const EAAccessoryDidDisconnectNotification;
EA_EXTERN NSString *const EAAccessoryKey;
EA_EXTERN NSString *const EAAccessorySelectedKey;

enum {
    EABluetoothAccessoryPickerAlreadyConnected,
    EABluetoothAccessoryPickerResultNotFound,
    EABluetoothAccessoryPickerResultCancelled,
    EABluetoothAccessoryPickerResultFailed
};
typedef NSInteger EABluetoothAccessoryPickerErrorCode;

typedef void (^EABluetoothAccessoryPickerCompletion)(NSError *error);

@interface EAAccessoryManager : NSObject

@property(nonatomic, readonly) NSArray *connectedAccessories;

+ (EAAccessoryManager *)sharedAccessoryManager;

#ifdef APPORTABLE
- (BOOL)isRegisteredForLocalNotifications;
#endif

- (void)showBluetoothAccessoryPickerWithNameFilter:(NSPredicate *)predicate completion:(EABluetoothAccessoryPickerCompletion)completion;
- (void)registerForLocalNotifications;
- (void)unregisterForLocalNotifications;

@end
