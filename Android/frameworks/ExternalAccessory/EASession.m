#import <ExternalAccessory/EASession.h>
#import <ExternalAccessory/EAAccessory.h>
#import <ExternalAccessory/BluetoothSession.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

NSString * const EASessionExceptionName = @"EASessionExceptionName";
NSString * const EASessionErrorDomain = @"EASessionErrorDomain";

typedef enum {
    EASessionConnectionError,
    EASessionInputStreamReadError,
    EASessionOutputStreamWriteError
} EASessionError;

@interface EASessionInputStream : NSInputStream

@property(nonatomic, readonly) BluetoothSession *bluetoothSession;

- (id)initWithBluetoothSession:(BluetoothSession *)session;
- (void)sendHasBytesAvailableEvent;

@end

@interface EASessionOutputStream : NSOutputStream

@property(nonatomic, readonly) BluetoothSession *bluetoothSession;

- (id)initWithBluetoothSession:(BluetoothSession *)session;
- (void)sendHasSpaceAvailableEvent;

@end

@interface EASession ()

@property(nonatomic, readwrite, strong) BluetoothSession *bluetoothSession;
@property(nonatomic, readwrite, strong) EAAccessory *accessory;
@property(nonatomic, readwrite, strong) NSString *protocolString;
@property(nonatomic, readwrite, strong) NSInputStream *inputStream;
@property(nonatomic, readwrite, strong) NSOutputStream *outputStream;

@end

@implementation EASession

- (id)initWithAccessory:(EAAccessory *)accessory forProtocol:(NSString *)protocolString
{
    self = [super init];
    if (self) {
        self.accessory = accessory;
        self.protocolString = protocolString;

        // Setup the Android Bluetooth session
        self.bluetoothSession = [BluetoothSession new];

        // Connect immediately
        [self.bluetoothSession connect:self.accessory.bluetoothDevice];

        // Block until the session has been connected
        while (1) {
            BluetoothSessionState state = (BluetoothSessionState)self.bluetoothSession.state;
            // Pass if connected
            if (BluetoothSessionStateConnected == state) {
                break;
            }
            // Bail if connecting fails
            else if (BluetoothSessionStateListen == state) {
                NSLog(@"EASession connecting failed");
                return nil;
            }
            // Connecting has been stopped
            else if (BluetoothSessionStateNone) {
                NSLog(@"EASession connecting canceled");
                return nil;
            }
            // Otherwise, continue
            else {
                continue;
            }
        }
       
        // Delegate bluetooth session callbacks to self
        self.bluetoothSession.delegate = (id<BluetoothSessionDelegate>)self;

        // Create streams to pipe data in and out of the session
        self.inputStream = [[EASessionInputStream alloc] initWithBluetoothSession:self.bluetoothSession];
        self.outputStream = [[EASessionOutputStream alloc] initWithBluetoothSession:self.bluetoothSession];
    }
    return self;
}

- (void)dealloc
{
    [self.bluetoothSession stop];
}

#pragma mark - Events

- (void)didConnect
{
    if ([self.accessory.manager isRegisteredForLocalNotifications]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter]
                postNotificationName:EAAccessoryDidConnectNotification
                object:self.accessory.manager
                userInfo:@{
                    EAAccessoryKey: self.accessory
                }];
        });
    }
}

- (void)didDisconnect
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.accessory.delegate accessoryDidDisconnect:self.accessory];
    });
    if ([self.accessory.manager isRegisteredForLocalNotifications]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter]
                postNotificationName:EAAccessoryDidDisconnectNotification
                object:self.accessory.manager
                userInfo:@{
                    EAAccessoryKey: self.accessory
                }];
        });
    }
}

@end

@implementation EASession (BluetoothSessionDelegate)

- (void)bluetoothSessionConnectionLost:(BluetoothSession *)session
{
    NSLog(@"EASession connection lost");
    [self.bluetoothSession stop];
    [self didDisconnect];
}

- (void)bluetoothSessionConnectionFailed:(BluetoothSession *)session
{
    NSLog(@"EASession connection failed");
}

- (void)bluetoothSessionHasBytesAvailable:(BluetoothSession *)session
{
    EASessionInputStream *inputStream = (EASessionInputStream *)self.inputStream;
    [inputStream sendHasBytesAvailableEvent];
}

- (void)bluetoothSessionHasSpaceAvailable:(BluetoothSession *)session
{
    EASessionOutputStream *outputStream = (EASessionOutputStream *)self.outputStream;
    [outputStream sendHasSpaceAvailableEvent];
}

@end

@interface EASessionInputStream () {
    NSStreamStatus _status;
    NSStreamEvent _event;
    NSError *_error;
}

@property (nonatomic, strong, readwrite) BluetoothSession *bluetoothSession;
@property (nonatomic, weak) id<NSStreamDelegate> delegate;

@end

@implementation EASessionInputStream

- (id)initWithBluetoothSession:(BluetoothSession *)session
{
    self = [super init];
    if (self) {
        self.bluetoothSession = session;
        _status = NSStreamStatusNotOpen;
        _event = NSStreamEventNone;
    }
    return self;
}

#pragma mark - NSStream Events

- (void)sendEvents
{
    if ([self.delegate respondsToSelector:@selector(stream:handleEvent:)]) {
        [self.delegate stream:self handleEvent:_event];
    }
}

- (void)sendHasBytesAvailableEvent
{
    _event |= NSStreamEventHasBytesAvailable;
    dispatch_after(0, dispatch_get_main_queue(), ^{
        [self sendEvents];
    });
}

#pragma mark - NSInputStream Requirements

// To create a subclass of NSInputStream you may have to implement initializers
// for the type of stream data supported and suitably re-implement existing initializers.
//
// You must also provide complete implementations of the following methods:

// From the current read index, take up to the number of bytes specified in the second parameter
// from the stream and place them in the client-supplied buffer (first parameter). The buffer must
// be of the size specified by the second parameter. Return the actual number of bytes placed in
// the buffer; if there is nothing left in the stream, return 0. Reset the index into the stream
// for the next read operation.
- (NSInteger)read:(uint8_t *)buffer maxLength:(NSUInteger)maxLength
{
    @try {
        NSData *data = [self.bluetoothSession read:maxLength];
        if (data.length == 0) {
            _event &= ~NSStreamEventHasBytesAvailable;
            return 0;
        }
        else {
            [data getBytes:buffer length:data.length];
            return data.length;
        }
    }
    @catch (NSException *ex) {
        NSLog(@"EASessionInputStream error %@ %@", ex.name, ex.reason);
        _event |= NSStreamEventErrorOccurred;
        _error = [NSError
            errorWithDomain:EASessionErrorDomain
            code:EASessionInputStreamReadError
            userInfo:nil];
        [self sendEvents];
    }
}

// Return in 0(1) a pointer to the subclass-allocated buffer (first parameter). Return by
// reference in the second parameter the number of bytes actually put into the buffer. The
// buffer’s contents are valid only until the next stream operation. Return NO if you cannot
// access data in the buffer; otherwise, return YES. If this method is not appropriate for your
// type of stream, you may return NO.
- (BOOL)getBuffer:(uint8_t **)buffer length:(NSUInteger *)len
{
    return NO;
}

// Return YES if there is more data to read in the stream, NO if there is not. If you want to
// be semantically compatible with NSInputStream, return YES if a read must be attempted to
// determine if bytes are available.
- (BOOL)hasBytesAvailable
{
    return YES;
}

#pragma - NSStream Requirements

- (NSStreamStatus)streamStatus
{
    return _status;
}

- (NSError *)streamError
{
    return _error;
}

- (void)open
{
    if (_status != NSStreamStatusNotOpen) {
        [NSException raise:EASessionExceptionName
            format:@"Stream has already been opened."];
    }
    _status = NSStreamStatusOpen;
}

- (void)close
{
    _status = NSStreamStatusClosed;
}

- (void)scheduleInRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode
{
    // NSRunLoop not needed. Stream events are "scheduled" by the
    // BluetoothSession callbacks.
}

- (void)removeFromRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode
{
    // NSRunLoop not needed.
}

- (void)dealloc
{
    [self close];
}

@end

@interface EASessionOutputStream () {
    NSStreamStatus _status;
    NSStreamEvent _event;
    NSError *_error;
}

@property (nonatomic, strong, readwrite) BluetoothSession *bluetoothSession;
@property (nonatomic, weak) id<NSStreamDelegate> delegate;

@end

@implementation EASessionOutputStream

- (id)initWithBluetoothSession:(BluetoothSession *)session
{
    self = [super init];
    if (self) {
        self.bluetoothSession = session;
        _status = NSStreamStatusNotOpen;
        _event = NSStreamEventNone;
    }
    return self;
}

#pragma mark - NSStream Events

- (void)sendEvents
{
    if ([self.delegate respondsToSelector:@selector(stream:handleEvent:)]) {
        [self.delegate stream:self handleEvent:_event];
    }
}

- (void)sendHasSpaceAvailableEvent
{
    _event |= NSStreamEventHasSpaceAvailable;
    [self sendEvents];
}

#pragma mark - NSOutputStream Requirements

// To create a subclass of NSOutputStream you may have to implement initializers for the
// type of stream data supported and suitably reimplement existing initializers. You must
// also provide complete implementations of the following methods:

// From the current write pointer, take up to the number of bytes specified in the
// maxLength: parameter from the client-supplied buffer (first parameter) and put them onto
// the stream. The buffer must be of the size specified by the second parameter. To prepare
// for the next operation, offset the write pointer by the number of bytes written. Return
// a signed integer based on the outcome of the current operation:

// If the write operation is successful, return the actual number of bytes put onto the stream.
// If there was an error writing to the stream, return -1.
// If the stream is of a fixed length and has reached its capacity, return zero.
- (NSInteger)write:(const uint8_t *)buffer maxLength:(NSUInteger)len
{
    @try {
        NSData *data = [NSData dataWithBytes:buffer length:len];
        [self.bluetoothSession write:data];
    }
    @catch (NSException *ex) {
        NSLog(@"EASessionOutputStream error %@ %@", ex.name, ex.reason);
        _event |= NSStreamEventErrorOccurred;
        _error = [NSError
            errorWithDomain:EASessionErrorDomain
            code:EASessionOutputStreamWriteError
            userInfo:nil];
        [self sendEvents];
        len = -1;
    }
    return len;
}

// Return YES if the stream can currently accept more data, NO if it cannot. If you want to be
// semantically compatible with NSOutputStream, return YES if a write must be attempted to determine
// if space is available.
- (BOOL)hasSpaceAvailable
{
    return YES;
}

#pragma mark - NSStream Requirements

- (NSStreamStatus)streamStatus
{
    return _status;
}

- (NSError *)streamError
{
    return _error;
}

- (void)open
{
    if (_status != NSStreamStatusNotOpen) {
        [NSException raise:EASessionExceptionName
            format:@"Stream has already been opened."];
    }
    _status = NSStreamStatusOpen;
}

- (void)close
{
    _status = NSStreamStatusClosed;
}

- (void)scheduleInRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode
{
    // NSRunLoop not needed. Stream events are "scheduled" by the
    // BluetoothSession callbacks.
}

- (void)removeFromRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode
{
    // NSRunLoop not needed.
}

- (void)dealloc
{
    [self close];
}

@end

#pragma clang diagnostic pop
