#import <ExternalAccessory/BluetoothSession.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation BluetoothSession

@synthesize delegate;

+ (void)initializeJava
{
    [super initializeJava];

    [BluetoothSession
        registerConstructor];

    [BluetoothSession
        registerInstanceMethod:@"start"
        selector:@selector(start)];

    [BluetoothSession
        registerInstanceMethod:@"stop"
        selector:@selector(stop)];

    [BluetoothSession
        registerInstanceMethod:@"connect"
        selector:@selector(connect:)
        returnValue:nil
        arguments:[AndroidBluetoothDevice className], nil];

    [BluetoothSession
        registerInstanceMethod:@"getState"
        selector:@selector(state)
        returnValue:[JavaClass intPrimitive]
        arguments:nil];

    [BluetoothSession
        registerInstanceMethod:@"read"
        selector:@selector(read:)
        returnValue:[NSData className]
        arguments:[JavaClass intPrimitive], nil];

    [BluetoothSession
        registerInstanceMethod:@"write"
        selector:@selector(write:)
        returnValue:nil
        arguments:[NSData className], nil];

    [BluetoothSession
        registerCallback:@"connectionLostCallback"
        selector:@selector(connectionLost)
        returnValue:nil
        arguments:nil];

    [BluetoothSession
        registerCallback:@"connectionFailedCallback"
        selector:@selector(connectionFailed)
        returnValue:nil
        arguments:nil];

    [BluetoothSession
        registerCallback:@"hasBytesAvailableCallback"
        selector:@selector(hasBytesAvailable)
        returnValue:nil
        arguments:nil];

    [BluetoothSession
        registerCallback:@"hasSpaceAvailableCallback"
        selector:@selector(hasSpaceAvailable)
        returnValue:nil
        arguments:nil];

    [BluetoothSession
        registerCallback:@"stateChangedCallback"
        selector:@selector(stateChanged)
        returnValue:nil
        arguments:nil];
}

- (void)connectionLost
{
    if ([self.delegate respondsToSelector:@selector(bluetoothSessionConnectionLost:)]) {
        [self.delegate bluetoothSessionConnectionLost:self];
    }
}

- (void)connectionFailed
{
    if ([self.delegate respondsToSelector:@selector(bluetoothSessionConnectionFailed:)]) {
        [self.delegate bluetoothSessionConnectionFailed:self];
    }
}

- (void)hasBytesAvailable
{
    if ([self.delegate respondsToSelector:@selector(bluetoothSessionHasBytesAvailable:)]) {
        [self.delegate bluetoothSessionHasBytesAvailable:self];
    }
}

- (void)hasSpaceAvailable
{
    if ([self.delegate respondsToSelector:@selector(bluetoothSessionHasSpaceAvailable:)]) {
        [self.delegate bluetoothSessionHasSpaceAvailable:self];
    }
}

- (void)stateChanged
{
    // TODO
}

+ (NSString *)className
{
    return @"com.modrobotics.externalAccessory.BluetoothSession";
}

@end

#pragma clang diagnostic pop