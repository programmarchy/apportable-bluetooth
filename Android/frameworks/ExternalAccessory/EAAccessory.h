#import <Foundation/Foundation.h>
#import <ExternalAccessory/ExternalAccessory.h>
#import <BridgeKit/AndroidBluetoothDevice.h>

@class EAAccessory;

@protocol EAAccessoryDelegate <NSObject>
@optional
- (void)accessoryDidDisconnect:(EAAccessory *)accessory;
@end

enum {
    EAConnectionIDNone = 0,
};

@interface EAAccessory : NSObject

#ifdef APPORTABLE
@property(nonatomic, readonly) AndroidBluetoothDevice *bluetoothDevice;
@property(nonatomic, readonly) EAAccessoryManager *manager;
#endif

@property(nonatomic, readonly, getter=isConnected) BOOL connected;
@property(nonatomic, readonly) NSUInteger connectionID;
@property(nonatomic, readonly) NSString *manufacturer;
@property(nonatomic, readonly) NSString *name;
@property(nonatomic, readonly) NSString *modelNumber;
@property(nonatomic, readonly) NSString *serialNumber;
@property(nonatomic, readonly) NSString *firmwareRevision;
@property(nonatomic, readonly) NSString *hardwareRevision;
@property(nonatomic, assign) id<EAAccessoryDelegate> delegate;

@property(nonatomic, readonly) NSArray *protocolStrings;

- (id)initWithBluetoothDevice:(AndroidBluetoothDevice *)bluetoothDevice manager:(EAAccessoryManager *)manager;

@end
