#ifdef __cplusplus
# define EA_EXTERN extern "C" __attribute__((visibility ("default")))
#else
# define EA_EXTERN extern __attribute__((visibility ("default")))
#endif

#define EA_METHOD_NOT_IMPLEMENTED() @throw([NSException \
    exceptionWithName:NSInvalidArgumentException \
    reason:[NSString stringWithFormat:@"method not implemented: %@", NSStringFromSelector(_cmd)] \
    userInfo:nil])