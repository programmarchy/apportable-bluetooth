#import <BridgeKit/JavaObject.h>
#import <BridgeKit/AndroidBluetoothDevice.h>

typedef enum {
    BluetoothSessionStateNone,
    BluetoothSessionStateListen,
    BluetoothSessionStateConnecting,
    BluetoothSessionStateConnected
} BluetoothSessionState;

@class BluetoothSession;

@protocol BluetoothSessionDelegate <NSObject>

- (void)bluetoothSessionConnectionLost:(BluetoothSession *)session;
- (void)bluetoothSessionConnectionFailed:(BluetoothSession *)session;
- (void)bluetoothSessionHasBytesAvailable:(BluetoothSession *)session;
- (void)bluetoothSessionHasSpaceAvailable:(BluetoothSession *)session;

@end

@interface BluetoothSession : JavaObject

@property(weak, nonatomic) id<BluetoothSessionDelegate> delegate;

- (void)start;
- (void)stop;
- (void)connect:(AndroidBluetoothDevice *)bluetoothDevice;
- (int)state;
- (void)write:(NSData *)data;
- (NSData *)read:(int)bytes;

@end
