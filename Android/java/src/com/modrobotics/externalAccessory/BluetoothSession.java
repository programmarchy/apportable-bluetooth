/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.modrobotics.externalAccessory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import java.util.ArrayList;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a thread that listens for
 * incoming connections, a thread for connecting with a device, and a
 * thread for performing data transmissions when connected.
 */
public class BluetoothSession {
    // Debugging
    private static final String TAG = "BluetoothSession";
    private static final boolean D = true;

    // SDP record when creating server socket
    private static final String SDP_NAME = "SPP";
    private static final UUID SDP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // Member fields
    private final BluetoothAdapter mAdapter;
    private AcceptThread mAcceptThread;
    private ConnectThread mConnectThread;
    private ReadThread mReadThread;
    private WriteThread mWriteThread;
    private int mState;

    // Callbacks
    private native void connectionFailedCallback();
    private native void connectionLostCallback();
    private native void hasBytesAvailableCallback();
    private native void hasSpaceAvailableCallback();
    private native void stateChangedCallback();

    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device

    public BluetoothSession() {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mState = STATE_NONE;
    }

    private synchronized void setState(int state) {
        if (D) Log.d(TAG, "setState() " + mState + " -> " + state);
        mState = state;
        stateChangedCallback();
    }

    public synchronized int getState() {
        return mState;
    }

    /**
     * Start the chat service. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume() */
    public synchronized void start() {
        if (D) Log.d(TAG, "start");

        // Cancel any other threads
        if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}
        if (mReadThread != null) {mReadThread.cancel(); mReadThread = null;}
        if (mWriteThread != null) {mWriteThread.cancel(); mWriteThread = null;}

        // Set state before starting listening thread
        setState(STATE_LISTEN);

        // Start the thread to listen on a BluetoothServerSocket
        if (mAcceptThread == null) {
            mAcceptThread = new AcceptThread();
            mAcceptThread.start();
        }
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     * @param device  The BluetoothDevice to connect
     */
    public synchronized void connect(BluetoothDevice device) {
        if (D) Log.d(TAG, "connect to: " + device);

        // Cancel any thread attempting to make a connection
        if (mState == STATE_CONNECTING) {
            if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}
        }

        // Cancel any existing read or write threads
        if (mReadThread != null) {mReadThread.cancel(); mReadThread = null;}
        if (mWriteThread != null) {mWriteThread.cancel(); mWriteThread = null;}

        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
        setState(STATE_CONNECTING);
    }

    /**
     * Start the ReadThread and WriteThread to begin managing a Bluetooth connection
     * @param socket  The BluetoothSocket on which the connection was made
     * @param device  The BluetoothDevice that has been connected
     */
    public synchronized void bind(BluetoothSocket socket, BluetoothDevice device) {
        if (D) Log.d(TAG, "bind");

        // Cancel the thread that completed the connection
        if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}

        // Cancel the accept thread because we only want to connect to one device
        if (mAcceptThread != null) {mAcceptThread.cancel(); mAcceptThread = null;}

        // Start the read thread to handle incoming data
        mReadThread = new ReadThread(socket);
        mReadThread.start();

        // Start the write thread for outgoing data
        mWriteThread = new WriteThread(socket);
        mWriteThread.start();

        setState(STATE_CONNECTED);
    }

    /**
     * Stop all threads
     */
    public synchronized void stop() {
        if (D) Log.d(TAG, "stop");
        if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}
        if (mReadThread != null) {mReadThread.cancel(); mReadThread = null;}
        if (mWriteThread != null) {mWriteThread.cancel(); mWriteThread = null;}
        if (mAcceptThread != null) {mAcceptThread.cancel(); mAcceptThread = null;}
        setState(STATE_NONE);
    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private void connectionFailed() {
        setState(STATE_LISTEN);
        connectionFailedCallback();
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private void connectionLost() {
        setState(STATE_LISTEN);
        connectionLostCallback();
    }

    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted
     * (or until cancelled).
     */
    private class AcceptThread extends Thread {
        // The local server socket
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            BluetoothServerSocket socket = null;
            try {
                socket = mAdapter.listenUsingRfcommWithServiceRecord(SDP_NAME, SDP_UUID);
            } catch (IOException e) {
                Log.e(TAG, "listening with rfcomm failed", e);
            }
            mmServerSocket = socket;
        }

        public void run() {
            if (D) Log.d(TAG, "BEGIN mAcceptThread" + this);
            setName("AcceptThread");
            BluetoothSocket socket = null;

            // Listen to the server socket if we're not connected
            while (mState != STATE_CONNECTED) {
                try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    Log.e(TAG, "accepting server socket failed", e);
                    break;
                }

                // If a connection was accepted
                if (socket != null) {
                    synchronized (BluetoothSession.this) {
                        switch (mState) {
                        case STATE_LISTEN:
                        case STATE_CONNECTING:
                            // Situation normal. Start the connected thread.
                            bind(socket, socket.getRemoteDevice());
                            break;
                        case STATE_NONE:
                        case STATE_CONNECTED:
                            // Either not ready or already connected. Terminate new socket.
                            try {
                                socket.close();
                            } catch (IOException e) {
                                Log.e(TAG, "could not close unwanted socket", e);
                            }
                            break;
                        }
                    }
                }
            }
            if (D) Log.d(TAG, "END mAcceptThread");
        }

        public void cancel() {
            if (D) Log.d(TAG, "cancel " + this);
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close of server failed", e);
            }
        }
    }

    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            mmDevice = device;
            BluetoothSocket socket = null;
            try {
                socket = device.createInsecureRfcommSocketToServiceRecord(SDP_UUID);
            } catch (IOException e) {
                Log.e(TAG, "creating rfcomm socket failed", e);
            }
            mmSocket = socket;
        }

        public void run() {
            if (D) Log.d(TAG, "BEGIN mConnectThread");
            setName("ConnectThread");

            // Always cancel discovery because it will slow down a connection
            mAdapter.cancelDiscovery();

            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                mmSocket.connect();
            } catch (IOException e) {
                connectionFailed();
                // Close the socket
                try {
                    mmSocket.close();
                } catch (IOException e2) {
                    Log.e(TAG, "unable to close socket during connection failure", e2);
                }
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (BluetoothSession.this) {
                mConnectThread = null;
            }

            // Start handling data
            bind(mmSocket, mmDevice);
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "closing socket failed", e);
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles incoming transmissions.
     */
    private class ReadThread extends Thread {
        private final InputStream mmInStream;
        private ArrayList<Byte> mmReadBuffer = new ArrayList<Byte>();
        private int mmLowWaterMark = 4;
        private volatile boolean mmRunning = true;

        public ReadThread(BluetoothSocket socket) {
            InputStream inStream = null;
            try {
                inStream = socket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "input stream not created", e);
            }
            mmInStream = inStream;
        }

        public void run() {
            if (D) Log.d(TAG, "BEGIN mReadThread");
            setName("ReadThread");

            byte[] buffer = new byte[1024];
            int bytes;

            // Keep listening to the InputStream while connected
            while (mmRunning) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);
                    synchronized (mmReadBuffer) {
                        for (int i = 0; i < bytes; ++i) {
                            mmReadBuffer.add(buffer[i]);
                        }
                    }
                    if (bytes > mmLowWaterMark) {
                        hasBytesAvailableCallback();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "read error", e);
                    connectionLost();
                    break;
                }
            }
        }

        /**
         * Read from the connected InStream's read buffer.
         * @param buffer  A buffer to read the bytes into.
         * @return The buffer that was read.
         */
        public byte[] read(int maxLength) {
            byte[] buffer = new byte[0];
            synchronized (mmReadBuffer) {
                int bufferSize = Math.min(maxLength, mmReadBuffer.size());
                buffer = new byte[bufferSize];
                for (int i = 0; i < bufferSize; ++i) {
                    buffer[i] = mmReadBuffer.get(i);
                }
                if (bufferSize > 0) {
                    mmReadBuffer.subList(0, bufferSize).clear();
                }
            }
            return buffer;
        }

        public void cancel() {
            mmRunning = false;
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles outgoing transmissions.
     */
    private class WriteThread extends Thread {
        private final OutputStream mmOutStream;
        private volatile boolean mmRunning = true;

        public WriteThread(BluetoothSocket socket) {
            OutputStream outStream = null;
            try {
                outStream = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "output stream not created", e);
            }
            mmOutStream = outStream;
        }

        public void run() {
            if (D) Log.d(TAG, "BEGIN mWriteThread");
            setName("WriteThread");
            while (mmRunning) {
                try {
                    Thread.sleep(100);
                }
                catch (InterruptedException e) {
                    Log.i(TAG, "write thread interrupted");
                }
            }
        }

        /**
         * Write to the connected OutStream.
         * @param buffer  The bytes to write
         */
        public void write(byte[] buffer) {
            try {
                mmOutStream.write(buffer);
            } catch (IOException e) {
                Log.e(TAG, "write error", e);
            }
        }

        public void cancel() {
            mmRunning = false;
        }
    }

    /**
     * Write to the WriteThread in an unsynchronized manner
     * @param out The bytes to write
     * @see WriteThread#write(byte[])
     */
    public void write(byte[] out) {
        WriteThread t;
        synchronized (this) {
            if (mState != STATE_CONNECTED) return;
            t = mWriteThread;
        }
        t.write(out);
    }

    /**
     * Read from the ReadThread
     * @param bytes The maximum number of bytes to read
     * @return The bytes read or null if an error occurred
     * @see ReadThread#read(int)
     */
    public byte[] read(int maxLength) {
        ReadThread t;
        synchronized (this) {
            if (mState != STATE_CONNECTED) return new byte[0];
            t = mReadThread;
        }
        return t.read(maxLength);
    }
}
