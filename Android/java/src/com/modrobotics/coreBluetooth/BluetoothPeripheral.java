package com.modrobotics.coreBluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class BluetoothPeripheral {
    // Debugging
    private static final String TAG = "BluetoothPeripheral";
    private static final boolean D = false;

    // Notification UUID
    private static final UUID NOTIFICATION_DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    // Member fields
    private final Activity mActivity;
    private final BluetoothDevice mDevice;
    private BluetoothGatt mGatt;
    private int mState;
    private ConcurrentLinkedQueue<Operation> mOperationQueue;

    // Constants that indicate the current state
    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;

    // Callbacks
    private native void servicesDiscoveredCallback(int status);
    private native void characteristicReadCallback(BluetoothGattCharacteristic characteristic, int status);
    private native void characteristicWriteCallback(BluetoothGattCharacteristic characteristic, int status);
    private native void characteristicChangedCallback(BluetoothGattCharacteristic characteristic);
    private native void characteristicNotificationUpdatedCallback(BluetoothGattCharacteristic characteristic, int status);
    private native void descriptorReadCallback(BluetoothGattDescriptor descriptor, int status);
    private native void descriptorWriteCallback(BluetoothGattDescriptor descriptor, int status);

    public BluetoothPeripheral(Activity activity, BluetoothDevice device) {
        mActivity = activity;
        mDevice = device;
        mState = STATE_DISCONNECTED;
        mOperationQueue = new ConcurrentLinkedQueue<Operation>();
    }

    public String getName() {
        // TODO: Parse the name from the scan record instead,
        // since it is more reliable.
        String deviceName = mDevice.getName();
        if (null == deviceName) {
            deviceName = mDevice.getAddress();
        }
        return deviceName;
    }

    protected BluetoothDevice getDevice() {
        return mDevice;
    }

    public List<BluetoothGattService> getServices() {
        return (null == mGatt) ? null : mGatt.getServices();
    }

    public static abstract class ConnectCallback {
        public abstract void onConnect();
        public abstract void onFailToConnect(int status);
        public abstract void onDisconnect(int status);
    }

    protected void connect(final ConnectCallback callback) {
        final BluetoothPeripheral peripheral = this;
        setState(STATE_CONNECTING);
        // Note: calling `connectGatt` on the main thread is necessary on certain Android devices (e.g. Samsung Galaxy S4)
        // See http://stackoverflow.com/questions/20069507/solved-gatt-callback-fails-to-register
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mGatt = mDevice.connectGatt(mActivity, false, new BluetoothGattCallback() {
                    @Override
                    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                        if (BluetoothProfile.STATE_CONNECTED == newState) {
                            if (BluetoothGatt.GATT_SUCCESS == status) {
                                setState(STATE_CONNECTED);
                                callback.onConnect();
                            }
                            else {
                                setState(STATE_DISCONNECTED);
                                callback.onFailToConnect(status);
                            }
                        }
                        else if (BluetoothProfile.STATE_DISCONNECTED == newState) {
                            setState(STATE_DISCONNECTED);
                            callback.onDisconnect(status);
                            mGatt.close();
                        }
                    }
                    @Override
                    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                        servicesDiscoveredCallback(status);
                    }
                    @Override
                    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                        characteristicReadCallback(characteristic, status);
                        dequeueOperation();
                    }
                    @Override
                    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                        if (BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE != characteristic.getWriteType()) {
                            characteristicWriteCallback(characteristic, status);
                            dequeueOperation();
                        }
                    }
                    @Override
                    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                        characteristicChangedCallback(characteristic);
                    }
                    @Override
                    public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                        descriptorReadCallback(descriptor, status);
                        dequeueOperation();
                    }
                    @Override
                    public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                        if (descriptor.getUuid().equals(NOTIFICATION_DESCRIPTOR_UUID)) {
                            characteristicNotificationUpdatedCallback(descriptor.getCharacteristic(), status);
                        }
                        else {
                            descriptorWriteCallback(descriptor, status);
                        }
                        dequeueOperation();
                    }
                });
            }
        });
    }

    protected void cancelConnection() {
        setState(STATE_DISCONNECTED);
        mGatt.disconnect();
    }

    private synchronized void setState(int state) {
        mState = state;
    }

    public synchronized int getState() {
        return mState;
    }

    public synchronized boolean isConnected() {
        return STATE_CONNECTED == mState;
    }

    public void discoverServices() {
        if (!mGatt.discoverServices()) {
            servicesDiscoveredCallback(BluetoothGatt.GATT_FAILURE);
        }
    }

    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        enqueueOperation(new ReadCharacteristicOperation(characteristic));
    }

    public void writeCharacteristic(BluetoothGattCharacteristic characteristic, byte[] value, int writeType) {
        final Operation writeOperation = new WriteCharacteristicOperation(characteristic, value, writeType);
        if (BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE == writeType) {
            writeOperation.execute();
        }
        else {
            enqueueOperation(writeOperation);
        }
    }

    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enabled) {
        if (mGatt.setCharacteristicNotification(characteristic, enabled)) {
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(NOTIFICATION_DESCRIPTOR_UUID);
            byte[] value = enabled ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE;
            enqueueOperation(new WriteDescriptorOperation(descriptor, value));
        }
        else {
            characteristicNotificationUpdatedCallback(characteristic, BluetoothGatt.GATT_FAILURE);
        }
    }

    public void readDescriptor(BluetoothGattDescriptor descriptor) {
        enqueueOperation(new ReadDescriptorOperation(descriptor));
    }

    public void writeDescriptor(BluetoothGattDescriptor descriptor, byte[] value) {
        enqueueOperation(new WriteDescriptorOperation(descriptor, value));
    }

    private void enqueueOperation(final Operation operation) {
        mOperationQueue.add(operation);
        if (mOperationQueue.size() == 1) {
            operation.execute();
        }
    }

    private void dequeueOperation() {
        if (null != mOperationQueue.poll()) {
            Operation nextOperation = mOperationQueue.poll();
            if (null != nextOperation) {
                nextOperation.execute();
            }
        }
    }

    protected abstract class Operation {
        public abstract void execute();
    }

    protected class ReadCharacteristicOperation extends Operation {
        private BluetoothGattCharacteristic mCharacteristic;

        public ReadCharacteristicOperation(BluetoothGattCharacteristic characteristic) {
            mCharacteristic = characteristic;
        }

        public void execute() {
            if (!mGatt.readCharacteristic(mCharacteristic)) {
                characteristicReadCallback(mCharacteristic, BluetoothGatt.GATT_FAILURE);
            }
        }
    }

    protected class WriteCharacteristicOperation extends Operation {
        private BluetoothGattCharacteristic mCharacteristic;
        private byte[] mValue;
        private int mWriteType;

        public WriteCharacteristicOperation(BluetoothGattCharacteristic characteristic, byte[] value, int writeType) {
            mCharacteristic = characteristic;
            mValue = value;
            mWriteType = writeType;
        }

        public void execute() {
            mCharacteristic.setWriteType(mWriteType);
            if (!mCharacteristic.setValue(mValue) ||
                !mGatt.writeCharacteristic(mCharacteristic)) {
                if (BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE != mWriteType) {
                    characteristicWriteCallback(mCharacteristic, BluetoothGatt.GATT_FAILURE);
                }
            }
        }
    }

    protected class ReadDescriptorOperation extends Operation {
        private BluetoothGattDescriptor mDescriptor;

        public ReadDescriptorOperation(BluetoothGattDescriptor descriptor) {
            mDescriptor = descriptor;
        }

        public void execute() {
            if (!mGatt.readDescriptor(mDescriptor)) {
                descriptorReadCallback(mDescriptor, BluetoothGatt.GATT_FAILURE);
            }
        }
    }

    protected class WriteDescriptorOperation extends Operation {
        private BluetoothGattDescriptor mDescriptor;
        private byte[] mValue;

        public WriteDescriptorOperation(BluetoothGattDescriptor descriptor, byte[] value) {
            mDescriptor = descriptor;
            mValue = value;
        }

        public void execute() {
            if (!mDescriptor.setValue(mValue) ||
                !mGatt.writeDescriptor(mDescriptor)) {
                descriptorWriteCallback(mDescriptor, BluetoothGatt.GATT_FAILURE);
            }
        }
    }
}