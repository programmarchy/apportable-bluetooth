package com.modrobotics.coreBluetooth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import com.modrobotics.coreBluetooth.BluetoothPeripheral;

public class BluetoothCentralManager {
    // Debugging
    private static final String TAG = "BluetoothCentralManager";
    private static final boolean D = false;

    // Member fields
    private final Activity mActivity;
    private final BluetoothAdapter mAdapter;
    private int mState;
    private boolean mScanning;

    // Callbacks
    private native void stateChangedCallback();
    private native void deviceDiscoveredCallback(BluetoothDevice device, int rssi, byte[] scanRecord);
    private native void deviceConnectedCallback(BluetoothDevice device);
    private native void deviceFailedToConnectCallback(BluetoothDevice device, int status);
    private native void deviceDisconnectedCallback(BluetoothDevice device, int status);

    // Constants that indicate the current state
    public static final int STATE_UNKNOWN = 0;
    public static final int STATE_UNAVAILABLE = 1;
    public static final int STATE_OFF = 2;
    public static final int STATE_ON = 3;
    
    public BluetoothCentralManager(Activity activity) {
        mActivity = activity;
        mAdapter = ((BluetoothManager)mActivity.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
        mState = STATE_UNKNOWN;
        mScanning = false;
    }

    private synchronized void setState(int state) {
        mState = state;
        stateChangedCallback();
    }

    public synchronized int getState() {
        return mState;
    }

    public synchronized void updateState() {
        if (mAdapter == null) {
            setState(STATE_UNAVAILABLE);
        }
        else if (!mAdapter.isEnabled()) {
            setState(STATE_OFF);
        }
        else {
            setState(STATE_ON);
        }
    }

    // Device scan callback
    private BluetoothAdapter.LeScanCallback mScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            deviceDiscoveredCallback(device, rssi, scanRecord);
        }
    };

    public void startScan() {
        if (mScanning) {
            return;
        }
        mScanning = true;
        mAdapter.startLeScan(mScanCallback);
    }

    public void stopScan() {
        if (!mScanning) {
            return;
        }
        mAdapter.stopLeScan(mScanCallback);
        mScanning = false;
    }

    public void connectPeripheral(BluetoothPeripheral peripheral) {
        final BluetoothDevice device = peripheral.getDevice();
        peripheral.connect(new BluetoothPeripheral.ConnectCallback() {
            @Override
            public void onConnect() {
                deviceConnectedCallback(device);
            }
            @Override
            public void onFailToConnect(int status) {
                deviceFailedToConnectCallback(device, status);
            }
            @Override
            public void onDisconnect(int status) {
                deviceDisconnectedCallback(device, status);
            }
        });
    }

    public void cancelPeripheralConnection(BluetoothPeripheral peripheral) {
        peripheral.cancelConnection();
    }
}